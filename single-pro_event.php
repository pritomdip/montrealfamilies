<?php
/**
 * The template for displaying all single posts.
 *
 * @package themeplate
 */

get_header(); ?>
<div class="wrapper" id="single-wrapper">

	<div id="content" class="container">

		<ul class="breadcrumb">
			<li>
				<a href="<?php echo get_site_url(); ?>">Montreal Famililes</a>
			</li>
			<li class="active">Montreal Famililes calendar</li>
		</ul>

		<div class="row">
			<div id="primary"
				 class="<?php if (is_active_sidebar('sidebar-5')) : ?>col-md-8<?php else : ?>col-md-12<?php endif; ?> content-area b-r-1">

				<main id="main" class="site-main" role="main">

					<?php while (have_posts()) : the_post(); ?>
					<?php get_template_part('loop-templates/event-single'); ?>
					<?php endwhile; ?>
					<?php get_template_part('page-templates/event-single-content') ?>
				</main><!-- #main -->

			</div><!-- #primary -->

			<div id="secondary" class="col-md-4 widget-area" role="complementary">

				<div class="sidebar calendar-sidebar">
					<?php dynamic_sidebar('sidebar-5'); ?>
					<!--event posts-->
				</div>

			</div><!-- #secondary -->


		</div><!-- .row -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>