<?php

/**
 * The template for displaying all category posts.
 *
 * @package themeplate
*/

global $excluded_posts, $current_category_id, $cat_name;

$current_category    = get_queried_object();
$current_category_id = $current_category->term_id;
$cat_name            = $current_category->name;

$parent_cat_arg         = array('hide_empty' => true, 'parent' => $current_category_id );
$multiple_child_cat     = get_terms('category', $parent_cat_arg);

get_header();

?>


	<div class="container category-page-wrapper">
		<div class="row">
			<div class="col-md-8">
				<?php 
					if(!empty($multiple_child_cat)){ 
						get_template_part( 'page-templates/category-slider' ); 
						get_template_part( 'page-templates/category-posts' ); 
					} else{ 
						get_template_part('page-templates/category-side-posts');
					} 
				?>
				
			</div>
		
			<div class="col-md-4 margin-t20">
				<?php 
					if(!empty($multiple_child_cat)){ 
						get_sidebar('sidebar-1'); 
					} else{ 
						dynamic_sidebar('sidebar-2');
					} 
				?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
