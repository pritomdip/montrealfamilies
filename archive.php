<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package themeplate
 */

get_header(); 

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
?>

<div class="wrapper" id="archive-wrapper">
    
    <div  id="content" class="container">

        <div class="row">
        
    	    <div id="primary" class="col-md-8 content-area">
               
                <main id="main" class="site-main" role="main">

                      <?php if ( have_posts() ) : ?>

                        <header class="directory-heading-wrapper">
                            <div class="directory-name-panel margin-t20">
                                <?php
                                    montreal_the_archive_title( '<h1 class="page-title directory-name">', '</h1>' );
                                ?>
                            </div>
                        </header><!-- .page-header -->

                        <div class="single-category-header-wrapper modules dotted-border">
                            <div class="left-title left-align">In This Issues</div>
                        </div>

                        <?php /* Start the Loop */ ?>
                        <?php while ( have_posts() ) : the_post(); ?>

                            <?php
                                /* Include the Post-Format-specific template for the content.
                                 * If you want to override this in a child theme, then include a file
                                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                 */
                                get_template_part( 'loop-templates/side', 'post' );
                            ?>

                        <?php endwhile; ?>

                        <div class="col-md-12 padding-lr5 text-center">
                            <ul class="pagination custom-pagination">  
                                <?php
                                  global $wp_query;
                                 
                                  $big = 999999999; // need an unlikely integer
                                  echo '<li class="paginate-links">';
                                    echo paginate_links( array(
                                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                    'format' => '?paged=%#%',
                                    'prev_text' => __('<span><i class="fa fa-angle-double-left"></i></span>'),
                                    'next_text' => __('<span><i class="fa fa-angle-double-right"></i></span>'),
                                    'current' => max( 1, get_query_var('paged') ),
                                    'total' => $wp_query->max_num_pages
                                    ) );
                                  echo '</li>';
                                ?>
                            </ul>
                        </div>

                             <?php // the_posts_navigation(); ?>

                        <?php else : ?>

                            <?php get_template_part( 'loop-templates/content', 'none' ); ?>

                        <?php endif; ?>

                </main><!-- #main -->
               
    	    </div><!-- #primary -->

            <div class="col-md-4">
                <?php get_sidebar(); ?>
            </div>

    </div> <!-- .row -->
        
    </div><!-- Container end -->
    
</div><!-- Wrapper end -->

<?php get_footer(); ?>
