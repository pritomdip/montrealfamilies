<?php
/*
 * Template Name:Home
 */
$excluded_posts = [];
get_header();
?>
<section class="body-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-8 body-border">
                <div class="banner-wrapper">
                    <?php get_template_part( 'page-templates/home-slider' ); ?>
                </div>
                <div class="mf-cat-posts">
                    <?php
                        echo do_shortcode('[mf_category_by_post cat="Community News" 
                        title="Community News" layout="grid"]');
                    ?>
                </div>

                <div class="mf-cat-posts">
                    <?php
                        echo do_shortcode('[mf_category_by_post cat="Upcoming Activities in Montreal" title="Upcoming Activities in Montreal" layout="grid"]');
                    ?>
                </div>

                <div class="mf-cat-posts">
                    <?php
                        //echo do_shortcode('[mf_category_by_post cat="Holiday Happenings" title="Holiday Happenings" limit="9" layout="grid"]');
                    ?>
                </div>

                <div class="mf-cat-posts">
                    <?php
                        echo do_shortcode('[mf_category_by_post cat="Winter Fun" title="Winter Fun" layout="grid"]');
                    ?>
                </div>

                <div class="mf-cat-posts">
                    <?php
                        echo do_shortcode('[mf_category_by_post cat="Features" title="Featured Articles" layout="grid"]');
                    ?>
                </div>

            </div>
            <div class="col-md-4 margin-t20">
                <?php get_sidebar('sidebar-1'); ?>
            </div>
            <?php //get_template_part( 'page-templates/home-sidebar' ); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
