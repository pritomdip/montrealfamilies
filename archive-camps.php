<?php
/**
 * The template for displaying Custom Post type pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package themeplate
 */
get_header(); 
?>
<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php
					get_template_part( 'page-templates/directory/directory-top-header' );
					get_template_part( 'page-templates/directory/directory-content' );
				?>
			</div>
			
			<div class="col-md-8 margin-t20">
				<div class="col-md-3">
					<?php get_template_part( 'page-templates/directory/directory-categories' ); ?>
				</div>
				<div class="col-md-9">
					<?php
						get_template_part( 'page-templates/directory/directory-posts' ); 
					?>
				</div>
			</div>
			<div class="col-md-4 margin-t20">
				<?php get_template_part( 'page-templates/google-map' );  ?>
				<div class="margin-t20">
					<?php dynamic_sidebar('sidebar-4'); ?>
				</div>
			</div>
			
		</div>
	</div>
</div>
<?php
get_footer();
?>