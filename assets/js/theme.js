window.Project = (function (window, document, $, undefined) {
    'use strict';

    var app = {
        initialize: function () {
        	console.log('working');

            app.initMatchHeight('.adjust_recommand_post_height');
            app.initFeatureSliderCarousel('.post-slider');
            app.initHomeSliderCarousel('.main-slider');
            app.alphabetFilter();
            app.makeFilter();
            app.makeMobileMenu();
            app.initCalendar();

            $('.height-style1, .height-style2, .height-style3, .height-style4').on('click', app.onClickChangeCss);


            $('.cat-slider-item').on('click', app.addNewClass);            
            $('.cat-slider-item').on('hover', app.addNewClassOnHover);                      
            $('.cat-slider-item').on('click', app.showSliderOnClick);
            $('.directory-btn').on('click', app.header_search_form);          
            $(document).on('click', '.listing-category,.listing-alphabet', app.onClickChangeCategoryName);
            $('.directory-filter-form select,.directory-filter-form input').on('change', app.onClickSubmitForm);

            $('.directory-search-toggle-btn').on('click', app.onClickshowFilterForm);
        },
        onClickChangeCss: function(selector){
            var abc = $(this).parents('.mobile-navbar').siblings('.mobile-navbar-expand').children(selector);
            
            $( abc ).each(function() {
                abc.removeClass('in');
            });

            console.log(abc);
        },
        onClickshowFilterForm: function(){

            $('.directory-slide').slideToggle('slow');
            
        },
        makeMobileMenu:function(){

            var mainmenu = $('#main-menu').clone(true);
            var secondarymenu = $('#secondary-menu').clone(true);
            var servicemenu = $('#menu-service-menu').clone(true);
            var search = $('#searchform').clone(true);
            
            $('.mobile-navbar-expand #collapse1').find('.well').append(mainmenu);
            $('.mobile-navbar-expand #collapse2').find('.well').append(secondarymenu);
            $('.mobile-navbar-expand #collapse3').find('.well').append(servicemenu);
            $('.mobile-navbar-expand #collapse4').find('.well').append(search);

        },
        initMatchHeight : function($selector){
        	$($selector).matchHeight({
                property: 'height' 
            });
        },
        initFeatureSliderCarousel : function($selector){
            $($selector).owlCarousel({
                items: 1,
                autoplay: true,
                loop: true,
                margin: 10,
                nav: false
            });
        }, 
        initHomeSliderCarousel : function($selector){
            $($selector).owlCarousel({
                items: 1,
                autoplay: true,
                dots: true,
                loop: true,
                autoHeight: true, 
                margin: 10
            });
        },
        showSliderOnClick: function(){ 
            var id = $(this).data('id'); 
            $('.post-slider').trigger('to.owl.carousel', id);
        },
        header_search_form: function () {
            $('#listing-filter-form').submit();
        },
        makeFilter: function () {
            var categories = $('.category_listing_select option');
            var category_lists = app.makeList(categories, 'category');
            $('ul.listing-categories').html(category_lists);
        },
        makeList: function (options, type) {
            var html = '';
            options.each(function (index, option) {
                var value = $(option).attr('value');
                var text = $(option).text();
                html += '<li data-type="' + type + '"data-value="' + value + '" class="listing-category listing-items">' + text + '</li>';
            });
            return html;
        },
        alphabetFilter: function () {
            var alphabets = $('.alphabet_listing_select option');
            var alphabet_lists = app.alphabetList(alphabets, 'alphabet');
            $('ul.listing-alphabets').html(alphabet_lists);
        },
        alphabetList: function (options, type) {
            var html = '';
            options.each(function (index, option) {
                var value = $(option).attr('value');
                var text = $(option).text();
                html += '<li data-type="' + type + '"data-value="' + value + '" class="listing-alphabet listing-items">' + text + '</li>';
            });
            return html;
        },
        onClickSubmitForm: function(){
            $('#listing-filter-form').submit();
        },
        onClickChangeCategoryName: function(){
            var type = $(this).data('type');
            var value = $(this).data('value');
            $('.' + type + '_listing_select').val(value).trigger('change');
        },
        addNewClass: function() {
            $('.cat-slider-item').removeClass('triangle');
            $(this).addClass('triangle');
        },
        addNewClassOnHover: function() {
            $('.cat-slider-item').removeClass('hover-triangle');
            $(this).addClass('hover-triangle');
        },
        initCalendar: function () {
            $('#community-calendar').fullCalendar();
        }
    };
 
    $(document).ready(app.initialize);
    
    return app; 
})(window, document, jQuery);
