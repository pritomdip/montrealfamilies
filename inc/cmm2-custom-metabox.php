<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'yourprefix_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Demo_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/CMB2/CMB2
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/CMB2/init.php';
}

/**
 * Conditionally displays a metabox when used as a callback in the 'show_on_cb' cmb2_box parameter
 *
 * @param  CMB2 $cmb CMB2 object.
 *
 * @return bool      True if metabox should show
 */
function kcparent_show_if_front_page( $cmb ) {
	// Don't show this metabox if it's not the front page template.
	if ( get_option( 'page_on_front' ) !== $cmb->object_id ) {
		return false;
	}
	return true;
}

/**
 * Conditionally displays a field when used as a callback in the 'show_on_cb' field parameter
 *
 * @param  CMB2_Field $field Field object.
 *
 * @return bool              True if metabox should show
 */
function kcparent_hide_if_no_cats( $field ) {
	// Don't show this field if not in the cats category.
	if ( ! has_tag( 'cats', $field->object_id ) ) {
		return false;
	}
	return true;
}

/**
 * Manually render a field.
 *
 * @param  array      $field_args Array of field arguments.
 * @param  CMB2_Field $field      The field object.
 */
function mf_render_row_cb( $field_args, $field ) {
	$classes     = $field->row_classes();
	$id          = $field->args( 'id' );
	$label       = $field->args( 'name' );
	$name        = $field->args( '_name' );
	$value       = $field->escaped_value();
	$description = $field->args( 'description' );
	?>
	<div class="custom-field-row <?php echo esc_attr( $classes ); ?>">
		<p><label for="<?php echo esc_attr( $id ); ?>"><?php echo esc_html( $label ); ?></label></p>
		<p><input id="<?php echo esc_attr( $id ); ?>" type="text" name="<?php echo esc_attr( $name ); ?>" value="<?php echo $value; ?>"/></p>
		<p class="description"><?php echo esc_html( $description ); ?></p>
	</div>
	<?php
}

/**
 * Manually render a field column display.
 *
 * @param  array      $field_args Array of field arguments.
 * @param  CMB2_Field $field      The field object.
 */
function mf_display_text_small_column( $field_args, $field ) {
	?>
	<div class="custom-column-display <?php echo esc_attr( $field->row_classes() ); ?>">
		<p><?php echo $field->escaped_value(); ?></p>
		<p class="description"><?php echo esc_html( $field->args( 'description' ) ); ?></p>
	</div>
	<?php
}

/**
 * Conditionally displays a message if the $post_id is 2
 *
 * @param  array      $field_args Array of field parameters.
 * @param  CMB2_Field $field      Field object.
 */
function mf_before_row_if_2( $field_args, $field ) {
	if ( 2 == $field->object_id ) {
		echo '<p>Testing <b>"before_row"</b> parameter (on $post_id 2)</p>';
	} else {
		echo '<p>Testing <b>"before_row"</b> parameter (<b>NOT</b> on $post_id 2)</p>';
	}
}



function mf_direcory_metabox(){
	$directory_metabox = new_cmb2_box(array(
		'id' =>  'address_setting',
		'title' => esc_html__('Address Section', 'themeplate'),
		'object_types' => array('babies','birthday','camps','childcare','course','edudirectoryion',					'family','familyservices','health','prepost','retail','needs','sports'),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	));

	$directory_metabox->add_field( array(
		'name' => 'Address',
		'id'   => 'address',
		'type' => 'text',
		'attributes'  => array(
			'placeholder' => '2205 Industriel Blvd.',
		),
	) );

	$directory_metabox->add_field( array(
		'name' => 'City',
		'id'   => 'city',
		'type' => 'text',
		'attributes'  => array(
			'placeholder' => 'Laval',
		)

	) );

	$directory_metabox->add_field( array(
		'name' => 'Phone',
		'id'   => 'phone',
		'type' => 'text',
		'attributes'  => array(
			'placeholder' => '450-669-2323',
		)
	) );

	$directory_metabox->add_field( array(
        'name'             => __( 'Listing Type', 'themeplate' ),
        'id'               => 'listingtype',
        'type'             => 'select',
        'show_option_none' => false,
        'default'          => 'free',
        'options'          => array(
            'free'     => __( 'Free', 'themeplate' ),
            'premium'  => __( 'Premium', 'themeplate' )
        ),
    ) );

    $directory_metabox->add_field( array(
        'name' => esc_html__( 'Images gallery', 'themeplate' ),
        'desc' => esc_html__( 'Upload max 3 images for Directory Images gallery.', 'themeplate' ),
        'id'   => '_directory_img_gallery',
        'type' => 'file_list',
    ) );

    $directory_metabox->add_field( array(
        'name'       => __( 'Longitude', 'themeplate' ),
        'id'         => 'longitude',
        'type'       => 'text',
        'attributes' => array(
            'placeholder' => '00.000000',
        ),
    ) );

    $directory_metabox->add_field( array(
        'name'       => __( 'latitude', 'themeplate' ),
        'id'         => 'latitude',
        'type'       => 'text',
        'attributes' => array(
            'placeholder' => '00.000000',
        ),
    ) );

    $directory_metabox->add_field( array(
        'name'       => __( 'County', 'themeplate' ),
        'id'         => 'county',
        'type'       => 'text',
        'attributes' => array(
            'placeholder' => 'Morris',
        ),
    ) );

    $directory_metabox->add_field( array(
        'name'       => __( 'Country', 'themeplate' ),
        'id'         => 'country',
        'type'       => 'text',
        'attributes' => array(
            'placeholder' => 'US',
        ),
    ) );

    $directory_metabox->add_field( array(
        'name'       => __( 'Other Address', 'themeplate' ),
        'id'         => 'address2',
        'type'       => 'text',
        'attributes' => array(
            'placeholder' => '2009 Palisade Ave.',
        ),
    ) );

    $directory_metabox->add_field( array(
        'name'       => __( 'Facebook URL', 'themeplate' ),
        'id'         => 'facebook',
        'type'       => 'text_url',
        'attributes' => array(
            'placeholder' => 'www.facebook.com',
        ),
    ) );

    $directory_metabox->add_field( array(
        'name'       => __( 'Twitter URL', 'themeplate' ),
        'id'         => 'twitter',
        'type'       => 'text_url',
        'attributes' => array(
            'placeholder' => 'www.twitter.com',
        ),
    ) );
	
}
add_action('cmb2_admin_init', 'mf_direcory_metabox');
