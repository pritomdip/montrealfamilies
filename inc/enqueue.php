<?php
/**
 * themeplate enqueue scripts
 *
 * @package themeplate
 */


function themeplate_scripts() {
	$version = defined('WP_DEBUG')? time(): '1.0.0';
    wp_enqueue_style( 'themeplate-styles', get_stylesheet_directory_uri() . '/assets/css/theme.css', array(), $version);
    wp_enqueue_style( 'owlcarousel', get_stylesheet_directory_uri() . '/assets/owlcarousel/css/owl.carousel.css', array(), '2.3.4' );
    wp_enqueue_script('jquery');
    wp_enqueue_script( 'google-map', "//maps.google.com/maps/api/js?sensor=false?key=AIzaSyCAL5P5fZMCVjsRhqXaVrmJStI-HyGZfh8", array( 'jquery' ), $version, true );
    wp_enqueue_script( 'owlcarousel', get_stylesheet_directory_uri() . '/assets/owlcarousel/js/owl.carousel.min.js', array( 'jquery' ), '2.3.4' );
    wp_enqueue_script( 'themeplate-scripts', get_template_directory_uri() . '/assets/js/theme.min.js', array('jquery'), $version, true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

add_action( 'wp_enqueue_scripts', 'themeplate_scripts' );
