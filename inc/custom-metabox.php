<?php

function themeplate_custom_meta() {
	add_meta_box( 'featured_posts', __( 'Post Properties', 'Themeplate' ), 'themeplate_render_metabox', 'post', 'side', 'high' );
	add_meta_box( 'slider_posts', __( 'Slider Properties', 'Themeplate' ), 'themeplate_render_slider_metabox', 'post', 'normal', 'high' );
	add_meta_box( 'subtitle_posts', __( 'Post Sub Heading & Author', 'Themeplate' ), 'themeplate_render_subtitle_metabox', 'post', 'normal', 'high' );
}


function themeplate_render_subtitle_metabox( $post ){

?>
	<div id="titlediv">
		<div id="titlewrap">			
			<?php 
				$subtitle      = get_post_meta( $post->ID, 'subtitle', true );
				$subtitle      = empty($subtitle) ? '' : $subtitle;
			?>
			<input type="text" name="subtitle" size="30" value="<?php echo $subtitle; ?>" placeholder="Sub Heading" id="title" spellcheck="true" autocomplete="off">
		</div>
	</div>

	<div id="titlediv">
		<div id="titlewrap">			
			<?php 
				$author      = get_post_meta( $post->ID, 'author', true );
				$author      = empty($author) ? '' : $author;
			?>
			<input type="text" name="author" size="30" value="<?php echo $author; ?>" placeholder="Author" id="title" spellcheck="true" autocomplete="off">
		</div>
	</div>
<?php
}

function themeplate_render_metabox( $post ) {
	$featured = get_post_meta( $post->ID );
	?>

	<p>
		<div class="row-content">
			<label for="featured-post">
				<input type="checkbox" name="featured_post" id="featured-post" value="true" <?php if ( isset ( $featured['_featured_post'] ) ) checked( $featured['_featured_post'][0], 'true' ); ?> />
				<?php _e( 'Featured', 'themeplate' )?>
			</label>
		</div>
	</p>
<?php

}

function themeplate_render_slider_metabox( $post ) {
	$featured = get_post_meta( $post->ID );
?>
	<p>
		<div class="row-content">
			<label for="has-slider-post">
				<?php _e( 'Slider Activate', 'themeplate' )?>
			</label>
			<input type="checkbox" name="has_slider" id="has-slider" value="true" <?php if ( isset ( $featured['_has_slider'] ) ) checked( $featured['_has_slider'][0], 'true' ); ?> />
		</div>
	</p>
	
		<div class="row-content">
			<label for="slide-post">
				<?php _e( 'Slide Post Link', 'themeplate' )?>
			</label>
			<?php 
				$slider      = get_post_meta( $post->ID, '_slide_post', true );
				$slider_link = empty($slider) ? '' : $slider;
			?>
			<input type="text" size="30" name="slide_post" id="slide-post" value="<?php echo $slider_link; ?>" />
		</div>
	
<?php

}

add_action( 'add_meta_boxes', 'themeplate_custom_meta' );

/**
 * Saves the post meta input
 */

function themeplate_save_metabox( $post_id ) {
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );

    if ( $is_autosave || $is_revision ) {
        return;
    }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		return;
	}

    $slide_post         = ! empty( $_POST['slide_post'] ) ? sanitize_text_field( $_POST['slide_post'] ) : '';
    $featured           = ! empty( $_POST['featured_post'] ) ? 'true' : 'false';
    $has_slider         = ! empty( $_POST['has_slider'] ) ? 'true' : 'false';
    $subtitle           = ! empty( $_POST['subtitle'] ) ? sanitize_text_field( $_POST['subtitle'] ) : '';
    $author             = ! empty( $_POST['author'] ) ? sanitize_text_field( $_POST['author'] ) : '';

	update_post_meta( $post_id, '_featured_post', $featured );
	update_post_meta( $post_id, '_slide_post', $slide_post );
	update_post_meta( $post_id, '_has_slider', $has_slider );
	update_post_meta( $post_id, 'subtitle', $subtitle );
	update_post_meta( $post_id, 'author', $author );

}
add_action( 'save_post', 'themeplate_save_metabox' );

?>
