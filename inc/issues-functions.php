<?php

 /*
  * Add Image in Category
 */

function montreal_issues_taxonomy() {
    /**
     *  Register Issues Category Taxonomy
     */
    register_taxonomy(
        'issues',
        'post',
        array(
            'hierarchical'      => true,
            'label'             => esc_html__( 'Issues', 'teemeplate' ),
            'query_var'         => true,
            'show_admin_column' => true,
            'rewrite'           => array(
                'slug'       => 'issues',
                'with_front' => true
            )
        )
    );

}

add_action( 'init', 'montreal_issues_taxonomy' );

