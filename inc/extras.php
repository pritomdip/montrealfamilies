<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package themeplate
 */
/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function themeplate_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}
	return $classes;
}
add_filter( 'body_class', 'themeplate_body_classes' );


function mf_get_all_post_types(){
	$directories = array(
		'babies'           => 'Babies',
		'birthday'         => 'Birthday Parties',
		'camps'            => 'Camps',
		'childcare'        => 'Childcare',
		'course'           => 'Courses & Programs',
		'education'		   => 'Education',
		'family'           => 'Family Fun',
		'familyservices'   => 'Family Services',
		'health'           => 'Health & Well Being',
		'prepost'          => 'Pro & Postnatal',
		'retail'           => 'Retail',
		'needs'            => 'Special Needs',
		'sports'           => 'Sports'
	);
	return $directories;
}


function get_directory_name($name){
	$types = mf_get_all_post_types();
	
    if (array_key_exists($name, $types)){
       return $types[$name]; 
    }
}

function themeplate_posted_byline() {
	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', 'themeplate' ),
		'<span class="author vcard"><span class="url fn n">' . esc_html( get_the_author() ) . '</span></span>'
	);

	echo '<span class="byline"> ' . $byline . '</span>';
}

//Archive Title

function montreal_the_archive_title( $before = '', $after = '' ) {
	$title = montreal_get_the_archive_title();

	if ( ! empty( $title ) ) {
		echo $before . $title . $after;
	}
}

function montreal_get_the_archive_title() {
	if ( is_category() ) {
		/* translators: Category archive title. 1: Category name */
		$title = sprintf( __( 'Category: %s' ), single_cat_title( '', false ) );
	} elseif ( is_tag() ) {
		/* translators: Tag archive title. 1: Tag name */
		$title = sprintf( __( 'Tag: %s' ), single_tag_title( '', false ) );
	} elseif ( is_author() ) {
		/* translators: Author archive title. 1: Author name */
		$title = sprintf( __( 'Author: %s' ), '<span class="vcard">' . get_the_author() . '</span>' );
	} elseif ( is_year() ) {
		/* translators: Yearly archive title. 1: Year */
		$title = sprintf( __( 'Year: %s' ), get_the_date( _x( 'Y', 'yearly archives date format' ) ) );
	} elseif ( is_month() ) {
		/* translators: Monthly archive title. 1: Month name and year */
		$title = sprintf( __( 'Month: %s' ), get_the_date( _x( 'F Y', 'monthly archives date format' ) ) );
	} elseif ( is_day() ) {
		/* translators: Daily archive title. 1: Date */
		$title = sprintf( __( '%s' ), get_the_date( _x( 'F , Y', 'daily archives date format' ) ) );
	} elseif ( is_tax( 'post_format' ) ) {
		if ( is_tax( 'post_format', 'post-format-aside' ) ) {
			$title = _x( 'Asides', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
			$title = _x( 'Galleries', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
			$title = _x( 'Images', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
			$title = _x( 'Videos', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
			$title = _x( 'Quotes', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
			$title = _x( 'Links', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
			$title = _x( 'Statuses', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
			$title = _x( 'Audio', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
			$title = _x( 'Chats', 'post format archive title' );
		}
	} elseif ( is_post_type_archive() ) {
		/* translators: Post type archive title. 1: Post type name */
		$title = sprintf( __( 'Archives: %s' ), post_type_archive_title( '', false ) );
	} elseif ( is_tax() ) {
		$tax = get_taxonomy( get_queried_object()->taxonomy );
		/* translators: Taxonomy term archive title. 1: Taxonomy singular name, 2: Current taxonomy term */
		$title = sprintf( __( '%1$s: %2$s' ), $tax->labels->singular_name, single_term_title( '', false ) );
	} else {
		$title = __( 'Archives' );
	}

	/**
	 * Filters the archive title.
	 *
	 * @since 4.1.0
	 *
	 * @param string $title Archive title to be displayed.
	 */
	return apply_filters( 'get_the_archive_title', $title );
}


function mf_set_post_views($postID){
    $count_key = 'mf_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0'); 
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

function mf_date_format($date){
	$date = strtotime($date);
	return date('M d,Y', $date);
}


function mf_date_to_day_name($from_date, $to_date){
	$from_date = new DateTime($from_date);
	$to_date   = new DateTime($to_date);
	ob_start();
	for ($date = $from_date; $date <= $to_date; $date->modify('+1 day')) {
		echo $date->format('l') . ", ";
	}
	$output = ob_get_clean();
	$days   = rtrim($output, ', ');
	echo $days . ".";
}