<?php

/**
 * Class ShortCode
 */

add_action('init', 'mf_render_all_shortcodes');

function mf_render_all_shortcodes(){
	add_shortcode('mf_category_by_post', 'render_mf_category_by_post');
	add_shortcode('mf_all_directories_post','render_mf_all_directories_post');
	add_shortcode('mf_all_categories_featured_post_by_cat_name','render_mf_all_categories_featured_post_by_cat_name');
	add_shortcode('mf_u_might_like', 'render_mf_u_might_like');
	add_shortcode('mf_get_all_directories', 'render_mf_get_all_directories');
	add_shortcode('mf_sidebar_newsletter', 'render_mf_sidebar_newsletter');
	add_shortcode('mf_sidebar_image_wrapper', 'render_mf_sidebar_image_wrapper');
	add_shortcode('mf_popular_posts', 'render_mf_popular_posts');
}

function render_mf_category_by_post($atts){
	$cat_atts = shortcode_atts(array(
		'title'  => '',
		'cat'    => '',
		'limit'  => 6,
		'layout' => 'grid',
		'type'   => 'post'
	), $atts, 'montrealfamilies');

	extract($cat_atts);
	ob_start();
	require get_template_directory() . '/loop-templates/post-categories-block.php';
	$html = ob_get_contents();
	ob_get_clean();

	return $html;
}

function render_mf_all_directories_post($atts){
	$directories = shortcode_atts(array(
		'title'      => '',
		'content'    => '',
		'layout'     => 'grid',
		'link'		 => '#',
		'image'      => ''
	), $atts, 'montrealfamilies');

	extract($directories);
	ob_start();
	require get_template_directory() . '/loop-templates/all-directories-block.php';
	$html = ob_get_contents();
	ob_get_clean();

	return $html;
}

function render_mf_all_categories_featured_post_by_cat_name($atts) {
	$directories = shortcode_atts(array(
		'layout'     => 'grid',
		'cat_name'	 => 'parties',
		'limit'		 => 12,
		'child_cat'	 => 'articles',
		'title'		 => 'Featured Articles'
	), $atts, 'montrealfamilies');

	extract($directories);
	ob_start();
	require get_template_directory() . '/loop-templates/categories-featured-post-slider.php';
	$html = ob_get_contents();
	ob_get_clean();

	return $html;
}

function render_mf_sidebar_parent_category ($atts){
	$types = shortcode_atts(array(
		'layout'     => 'grid',
		'cat_name'	 => 'courses',
		'title'		 => 'Courses'
	), $atts, 'montrealfamilies');

	extract($types);
	ob_start();
	require get_template_directory() . '/loop-templates/sidebar-parent-cat.php';
	$html = ob_get_contents();
	ob_get_clean();

	return $html;
}

function render_mf_u_might_like($atts){
	$cat_atts = shortcode_atts(array(
		'limit'  => 3,
	), $atts, 'montrealfamilies');

	extract($cat_atts);
	ob_start();
	require get_template_directory() . '/loop-templates/u-might-like-block.php';
	$html = ob_get_contents();
	ob_get_clean();

	return $html;
}


function render_mf_get_all_directories($atts){
	ob_start();
	require get_template_directory() . "/page-templates/sidebar-cat-directories.php";
	$html = ob_get_contents();
	ob_get_clean();
	return $html;
}

function render_mf_sidebar_newsletter($atts){
	ob_start();
	require get_template_directory() . "/page-templates/sidebar-newsletter.php";
	$html = ob_get_contents();
	ob_get_clean();
	return $html;
}

function render_mf_sidebar_image_wrapper($atts){
	ob_start();
	require get_template_directory() . "/page-templates/sidebar-image-wrapper.php";
	$html = ob_get_contents();
	ob_get_clean();
	return $html;
}

function render_mf_popular_posts($atts){
	ob_start();
	require get_template_directory() . "/page-templates/sidebar-popular-posts.php";
	$html = ob_get_contents();
	ob_get_clean();
	return $html;
}
