<?php

class Directories
{

	protected static $instance = null;

	/**
	 * PostTypes constructor.
	 */
	public function __construct()
	{
		add_action('init', array($this, 'register_post_types'));
		add_action('init', array($this, 'register_taxonomies'));
	}

	public static function instance()
	{
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Register custom post types
	 */
	public function register_post_types()
	{
		register_post_type('babies', array(
			'labels' => $this->get_posts_labels('Babies', __('Babies', 'themeplate'), __('Babies', 'themeplate')),
			'hierarchical' => false,
			'supports'     => array('title', 'editor', 'thumbnail'),
			'public'       => true,
			'show_ui'      => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 4,
			'menu_icon' => 'dashicons-category',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,		
			'rewrite' => true,
			'capability_type' => 'post'

		));

		register_post_type('birthday', array(
			'labels' => $this->get_posts_labels('Birthday Parties', __('Birthday Parties', 'themeplate'), __('Birthday Parties', 'themeplate')),
			'hierarchical' => false,
			'supports' => array('title', 'editor', 'thumbnail'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-category',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post'

		));

		register_post_type('camps', array(
			'labels' => $this->get_posts_labels('Camps', __('Camps', 'themeplate'), __('Camps', 'themeplate')),
			'hierarchical' => false,
			'supports' => array('title', 'editor', 'thumbnail'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-category',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post',
		));

		register_post_type('childcare', array(
			'labels' => $this->get_posts_labels('Childcare', __('Childcare', 'themeplate'), __('Childcare', 'themeplate')),
			'hierarchical' => false,
			'supports' => array('title', 'editor', 'thumbnail'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-category',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post',
		));

		register_post_type('course', array(
			'labels' => $this->get_posts_labels('Courses & Programs', __('Courses & Programs', 'themeplate'), __('Courses & Programs', 'themeplate')),
			'hierarchical' => false,
			'supports' => array('title', 'editor', 'thumbnail'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-category',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post',
		));

		register_post_type('education', array(
			'labels' => $this->get_posts_labels('Education', __('Education', 'themeplate'), __('Education', 'themeplate')),
			'hierarchical' => false,
			'supports' => array('title', 'editor', 'thumbnail'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-category',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post',
		));

		register_post_type('family', array(
			'labels' => $this->get_posts_labels('Family Fun', __('Family Fun', 'themeplate'), __('Family Fun', 'themeplate')),
			'hierarchical' => false,
			'supports' => array('title', 'editor', 'thumbnail'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-category',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post',
		));

		register_post_type('familyservices', array(
			'labels' => $this->get_posts_labels('Family Services', __('Family Services', 'themeplate'), __('Family Services', 'themeplate')),
			'hierarchical' => false,
			'supports' => array('title', 'editor', 'thumbnail'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-category',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post',
		));

		register_post_type('health', array(
			'labels' => $this->get_posts_labels('Health & Well Being', __('Health & Well Being', 'themeplate'), __('Health & Well Being', 'themeplate')),
			'hierarchical' => false,
			'supports' => array('title', 'editor', 'thumbnail'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-category',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post',
		));

		register_post_type('prepost', array(
			'labels' => $this->get_posts_labels('Pre & Postnatal', __('Pre & Postnatal', 'themeplate'), __('Pre & Postnatal', 'themeplate')),
			'hierarchical' => false,
			'supports' => array('title', 'editor', 'thumbnail'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-category',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post',
		));

		register_post_type('retail', array(
			'labels' => $this->get_posts_labels('Retail', __('Retail', 'themeplate'), __('Retail', 'themeplate')),
			'hierarchical' => false,
			'supports' => array('title', 'editor', 'thumbnail'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-category',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post',
		));

		register_post_type('needs', array(
			'labels' => $this->get_posts_labels('Special Needs', __('Special Needs', 'themeplate'), __('Special Needs', 'themeplate')),
			'hierarchical' => false,
			'supports' => array('title', 'editor', 'thumbnail'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-category',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post',
		));

		register_post_type('sports', array(
			'labels' => $this->get_posts_labels('Sports', __('Sports', 'themeplate'), __('Sports', 'themeplate')),
			'hierarchical' => false,
			'supports' => array('title', 'editor', 'thumbnail'),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-category',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => true,
			'capability_type' => 'post',
		));

	}


	/**
	 * Register custom taxonomies
	 *
	 * @since 1.0.0
	 */
	public function register_taxonomies()
	{
		register_taxonomy('babies_category', array('babies'), array(
			'labels' => $this->get_taxonomy_label('Categories', __('Babies Category', 'themeplate'), __('Babies Categories', 'themeplate')),
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => true,
			'rewrite' => array('slug' => 'babies_category'),
		));

		register_taxonomy('birthday_category', array('birthday'), array(
			'labels' => $this->get_taxonomy_label('Categories', __('Birthday Category', 'themeplate'), __('Birthday Categories', 'themeplate')),
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => true,
			'query_var' => true,
			'rewrite' => array('slug' => 'birthday_category',
				'with_front' => true),
		));

		register_taxonomy('camps_category', array('camps'), array(
			'labels' => $this->get_taxonomy_label('Categories', __('Camps Category', 'themeplate'), __('Camps Categories', 'themeplate')),
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => true,
			'rewrite' => array('slug' => 'camps_category'),
		));

		register_taxonomy('childcare_category', array('childcare'), array(
			'labels' => $this->get_taxonomy_label('Categories', __('Childcare Category', 'themeplate'), __('Childcare Categories', 'themeplate')),
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => true,
			'rewrite' => array('slug' => 'childcare_category'),
		));

		register_taxonomy('course_category', array('course'), array(
			'labels' => $this->get_taxonomy_label('Categories', __('Courses & Programs Category', 'themeplate'), __('Courses & Programs Categories', 'themeplate')),
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => true,
			'rewrite' => array('slug' => 'course_category'),
		));

		register_taxonomy('education_category', array('education'), array(
			'labels' => $this->get_taxonomy_label('Categories', __('Education Category', 'themeplate'), __('Education Categories', 'themeplate')),
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => true,
			'rewrite' => array('slug' => 'education_category'),
		));

		register_taxonomy('family_category', array('family'), array(
			'labels' => $this->get_taxonomy_label('Categories', __('Family Fun Category', 'themeplate'), __('Family Fun Categories', 'themeplate')),
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => true,
			'rewrite' => array('slug' => 'family_category'),
		));

		register_taxonomy('familyservices_category', array('familyservices'), array(
			'labels' => $this->get_taxonomy_label('Categories', __('Family Services Category', 'themeplate'), __('Family Services Categories', 'themeplate')),
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => true,
			'rewrite' => array('slug' => 'familyservices_category'),
		));
		
		register_taxonomy('health_category', array('health'), array(
			'labels' => $this->get_taxonomy_label('Categories', __('Health & Well Being Category', 'themeplate'), __('Health & Well Being Categories', 'themeplate')),
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => true,
			'rewrite' => array('slug' => 'health_category'),
		));

		register_taxonomy('prepost_category', array('prepost'), array(
			'labels' => $this->get_taxonomy_label('Categories', __('Pre & Postnatal Category', 'themeplate'), __('Pre & Postnatal Categories', 'themeplate')),
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => true,
			'rewrite' => array('slug' => 'prepost_category'),
		));

		register_taxonomy('retail_category', array('retail'), array(
			'labels' => $this->get_taxonomy_label('Categories', __('Retail Category', 'themeplate'), __('Retail Categories', 'themeplate')),
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => true,
			'rewrite' => array('slug' => 'retail_category'),
		));

		register_taxonomy('needs_category', array('needs'), array(
			'labels' => $this->get_taxonomy_label('Categories', __('Needs Category', 'themeplate'), __('Needs Categories', 'themeplate')),
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => true,
			'rewrite' => array('slug' => 'needs_category'),
		));

		register_taxonomy('sports_category', array('sports'), array(
			'labels' => $this->get_taxonomy_label('Categories', __('Sports Category', 'themeplate'), __('Sports Categories', 'themeplate')),
			'hierarchical' => true,
			'public' => true,
			'show_admin_column' => true,
			'rewrite' => array('slug' => 'sports_category'),
		));

	}

	/**
	 * Get all labels from post types
	 *
	 * @param $menu_name
	 * @param $singular
	 * @param $plural
	 *
	 * @return array
	 * @since 1.0.0
	 */
	protected static function get_posts_labels($menu_name, $singular, $plural)
	{
		$labels = array(
			'name'          	 => $singular,
			'all_items'			 => sprintf(__("All %s", 'themeplate'), $plural),
			'singular_name' 	 => $singular,
			'add_new'			 => sprintf(__('New %s', 'themeplate'), $singular),
			'add_new_item'		 => sprintf(__('Add New %s', 'themeplate'), $singular),
			'edit_item'			 => sprintf(__('Edit %s', 'themeplate'), $singular),
			'new_item'			 => sprintf(__('New %s', 'themeplate'), $singular),
			'view_item' 		 => sprintf(__('View %s', 'themeplate'), $singular),
			'search_items' 		 => sprintf(__('Search %s', 'themeplate'), $plural),
			'not_found' 		 => sprintf(__('No %s found', 'themeplate'), $plural),
			'not_found_in_trash' => sprintf(__('No %s found in Trash', 'themeplate'), $plural),
			'parent_item_colon'  => sprintf(__('Parent %s:', 'themeplate'), $singular),
			'menu_name'          => $menu_name,
		);

		return $labels;
	}

	/**
	 * Get all labels from taxonomies
	 *
	 * @param $menu_name
	 * @param $singular
	 * @param $plural
	 *
	 * @return array
	 * @since 1.0.0
	 */
	protected static function get_taxonomy_label($menu_name, $singular, $plural)
	{
		$labels = array(
			'name' => sprintf(_x('%s', 'taxonomy general name', 'themeplate'), $plural),
			'singular_name' => sprintf(_x('%s', 'taxonomy singular name', 'themeplate'), $singular),
			'search_items' => sprintf(__('Search %', 'themeplate'), $plural),
			'all_items' => sprintf(__('All %s', 'themeplate'), $plural),
			'parent_item' => sprintf(__('Parent %s', 'themeplate'), $singular),
			'parent_item_colon' => sprintf(__('Parent %s:', 'themeplate'), $singular),
			'edit_item' => sprintf(__('Edit %s', 'themeplate'), $singular),
			'update_item' => sprintf(__('Update %s', 'themeplate'), $singular),
			'add_new_item' => sprintf(__('Add New %s', 'themeplate'), $singular),
			'new_item_name' => sprintf(__('New % Name', 'themeplate'), $singular),
			'menu_name' => __($menu_name, 'themeplate'),
		);

		return $labels;
	}
}


function montreal_directories()
{
	return Directories::instance();
}

montreal_directories();
