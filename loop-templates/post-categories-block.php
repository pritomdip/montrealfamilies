<?php
/**
 * @package themeplate
 */
?>

<?php
$post_args     = array(
	'category_name'  => $cat,
	'posts_per_page' => $limit,
	'post_type'      => $type
);
$query         = new WP_Query($post_args);
$category_id   = get_cat_ID($cat);
$category_link = get_category_link($category_id); 
global $post;
?>
<div class="post-card-list module">
	<h2 class="post-type-name">
		<?php if (!empty($title)) echo $title; else echo $cat; ?>
		<a class="btn archive pull-right more-links-btn" href="<?php echo esc_url($category_link); ?>">More</a>
	</h2>
	<div class="row margin-lr5">
		<?php
		while ($query->have_posts()) : $query->the_post(); 
			$id = $post->ID; 

		 if (!empty($layout) && $layout == 'grid') { ?>
			<div class="col-md-4 col-sm-4 col-xs-6 padding-lr5">
				<div class="post-card adjust_recommand_post_height padding-10 border-1 radius-5 margin-b10">
					<div class="post-card-top">
						<a href="<?php echo get_the_permalink(); ?>">
							<?php the_post_thumbnail('blog-thumb'); ?>
						</a>
					</div>
					<div class="post-card-bottom">
						<?php the_title(sprintf('<h4 class="post-card-title  margin-t10"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h4>'); ?>
						<div class="post-card-content">
							<?php
								$content = get_the_content();
								echo wp_trim_words($content, 20, '');
							?>
						</div>
					</div>
				</div>
			</div>
		<?php } 
		endwhile;
		wp_reset_postdata();
		?>
	</div>
</div>