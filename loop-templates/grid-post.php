<div class="col-md-4 col-sm-4 col-xs-6 padding-lr5">
	<div class="post-card adjust_recommand_post_height padding-10 border-1 radius-5 margin-b10">
		<div class="post-card-top">
			<a href="<?php echo get_the_permalink($post); ?>">
				<?php the_post_thumbnail('blog-thumb'); ?>
			</a>
		</div>
		<div class="post-card-bottom">
			<?php the_title(sprintf('<h4 class="post-card-title  margin-t10"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h4>'); ?>
			<div class="post-card-content">
				<?php
					$content = get_the_content();
					echo wp_trim_words($content, 20, '');
				?>
			</div>
		</div>
	</div>
</div>