<?php
global $post;

$subtitle = get_post_meta($post->ID, 'subtitle', true);

if(empty($subtitle)){
	$subtitle = '';
}

?>


<div class="post-card-list modules">
	<div class="post-card-top">
		<a href="<?php echo get_the_permalink($post); ?>">
			<?php the_post_thumbnail('thumbnail'); ?>
		</a>
	</div>
	<div class="post-card-bottom">
		<div class="bottom-left">
			<?php the_title(sprintf('<h4 class="post-card-title  margin-t10"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h4>'); ?>
		</div>
		<div class="post-card-content">
			<?php
				echo $subtitle;
			?>
		</div>
		<?php
			$author = get_post_meta($post->ID, 'author', true);
			if(!empty( $author )){
		?>
			<div class="author"><?php echo $author; ?></div>
		<?php
			}
		?>
		
	</div>
</div>