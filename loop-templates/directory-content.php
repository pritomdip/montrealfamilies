<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package themeplate
 */
global $post;

$id 		   = $post->ID;
$post_metas    = get_post_meta( $id );

$address  	   = isset($post_metas['address']) ? $post_metas['address'][0] : '';
$address2  	   = isset($post_metas['address2']) ? $post_metas['address2'][0] : '';
$city  		   = isset($post_metas['city']) ? $post_metas['city'][0] : '';
$phone  	   = isset($post_metas['phone']) ? $post_metas['phone'][0] : '';
$listingtype   = isset($post_metas['listingtype']) ? $post_metas['listingtype'][0] : '';
$zipcode       = isset($post_metas['zipcode']) ? $post_metas['zipcode'][0] : '' ;
$state    	   = isset($post_metas['state']) ? $post_metas['state'][0] : '' ;

if( !empty( $listingtype ) && $listingtype == "premium" ){
	$class="premium";
} else{
	$class="";
}

$post_type = get_post_type_object(get_post_type());
if ($post_type) {
    $directory_name = strtolower(esc_html($post_type->labels->singular_name));
    $directory      = strtolower(esc_html($post_type->name));
}

?>

<div class="<?php echo $class; ?> directory-single-list parent-flex padding-10">
	<div class="directory-single-list-top child-flex">
		<h4 class="directory-single-list-name">
			<a href="<?php echo get_the_permalink($post); ?>"><?php echo get_the_title($post); ?></a>
		</h4>
		<div class="directory-single-content-list">
			<?php if (!empty($address)) echo "<p>$address</p>"; ?>
			<?php if (!empty($address2)) echo "<p>$address2</p>"; ?>
			<?php if (!empty($city)) echo "<p>$city<span>, ". $state ."</span> <span> ". $zipcode ."</span></p>"; ?>
			<?php if (!empty($phone)) echo "<p>$phone</p>"; ?>
		</div>
	</div>
	<div class="directory-single-category child-flex">
		<?php 
			$post_categories = get_the_terms($post->ID, $directory . '_category');

			if (!empty($post_categories) && !is_wp_error($post_categories)) {
				$categories = wp_list_pluck($post_categories, 'name', 'term_id');
				?>
				<div class="directory-list-cat">
					<span class="directory-cat">Category: </span>
					<span class="directory-cat-name"><?php echo implode(", ", $categories); ?></span>
				</div>

		<?php }	?>

	</div>
	<?php if ( has_post_thumbnail() ){ ?>
		<div class="directory-single-list-image child-flex">	
			<a href="<?php echo get_the_permalink($post); ?>">
				<?php echo get_the_post_thumbnail( $id, 'thumbnail' ); ?>
			</a>
		</div>
	<?php } ?>
</div>
