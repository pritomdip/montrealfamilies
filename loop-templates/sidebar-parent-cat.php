<div class="sidebar-resource">
    <div class="post-type-name"><?php echo $title; ?></div>
    <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
    <?php
       $directory_names = mf_get_all_post_types();         
       foreach( $directory_names as $key => $value) :
    ?>
        <!--- START PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="faq-heading-<?php echo $key; ?>">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-target="#<?php echo $key; ?>"
                       data-parent="#<?php echo $key; ?>2" aria-expanded="true" aria-controls="<?php echo $key; ?>"
                       class=""><?php echo $value; ?> <i class="fa fa-caret-down sidebar-caret-icon" aria-hidden="true"></i>
                   </a>
                </h4>
            </div>
            <div id="<?php echo $key; ?>" class="panel-collapse collapse" role="tabpanel"
                 aria-labelledby="faq-heading-<?php echo $key; ?>" aria-expanded="true" style="">
                <div class="panel-body">
                    <ul class="list-unstyled">
                    <?php
                        $taxonomy = $key.'_category'; 
                        $terms = get_terms($taxonomy, array(
                                    'hide_empty' => false,
                                ));
                       
                        if ( $terms && !is_wp_error( $terms ) ) {
                            foreach ( $terms as $term ) { ?>
                                <li><a href="<?php echo get_term_link($term->slug, $taxonomy); ?>"><?php echo $term->name; ?></a>
                                </li>
                    <?php   }    
                       } 
                    ?>
                    </ul>
                </div>
            </div>
        </div>
        <!--   END PANEL -->
        <?php endforeach; ?>
    </div>
</div> 