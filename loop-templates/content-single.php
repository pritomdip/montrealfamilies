<?php
/**
 * @package themeplate
 */
global $post;
$subtitle = get_post_meta($post->ID, 'subtitle', true);

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <header class="entry-header">

		<?php the_title( '<h2 class="entry-title">', '</h2>' ); 

			if(!empty($subtitle)){
		?>
			<h2 class="entry-subtitle"><?php echo $subtitle; ?></h2>
		<?php } ?>

        <div class="entry-meta">

			<?php themeplate_posted_on(); ?>

        </div><!-- .entry-meta -->

    </header><!-- .entry-header -->

    <div class="single-image-wrapper">
		<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>
	</div>

    <div class="entry-content">

		<?php the_content(); ?>

		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'themeplate' ),
			'after'  => '</div>',
		) );
		?>

    </div><!-- .entry-content -->

</article><!-- #post-## -->
