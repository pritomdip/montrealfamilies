<?php
/**
* @package themeplate
*/

$multiple_child_cat = explode(",", $child_cat); 

$excluded_posts = [];
$parent_cat_id = get_cat_ID( $cat_name );
	$post_args = array(
					'posts_per_page'   => 7,
					'category_name'    => $cat_name,
					'orderby'          => 'publish_date',
					'order'            => 'DESC',
					'exclude'          =>  $excluded_posts,
					'meta_key'         => '_featured_post',
					'meta_value'       => 'true',
					'post_type'        => 'post',
					'post_status'      => 'publish',
				 );

	$posts = get_posts( $post_args ); 

?>
<div class="col-md-7 margin-t20 slider-parent">
	<div class="post-slider owl-carousel">
	<?php
		foreach ( $posts as $post ) {
		    setup_postdata($post);
			$excluded_posts[]   = $post->ID;	
	?>

		<div class="slider-image-wrapper <?php echo $post->ID; ?>" data-id="<?php echo $post->ID; ?>">
			<a href="<?php echo get_the_permalink($post); ?>">
				<?php echo get_the_post_thumbnail( $post->ID, 'slider-thumbnail' ); ?>
			</a>
		</div>
<!-- Slider pannel ends Here -->

		<?php
			}
		?>
	</div>
</div>

<div class="col-md-5 margin-t20 slider-content-title">
	<ul class="mf-cat-sllider">
	<?php
	
	foreach ( $posts as $post ) {
	    setup_postdata($post);
		$excluded_posts[]   = $post->ID;
		
	?>
		<li class="cat-slider-item" data-id="<?php echo $post->ID; ?>"><?php echo get_the_title($post); ?>
		</li>
<?php }  ?>
	</ul>
</div>

<?php

foreach ($multiple_child_cat as $child_cat) {

	$args = [
	      'post_type'      => 'post', 
	      'orderby'        => 'publish_date', 
	      'order'          => 'DESC',
	      'category_name'  =>  $child_cat,
	      'exclude'        =>  $excluded_posts, 
	      'posts_per_page' =>  $limit,
	];

	$posts         = get_posts( $args );
	$category_id   = get_cat_ID($child_cat);
	$category_link = get_category_link($category_id);

	if(!empty($posts)) {

?>
	<div class="col-md-12">
		<div class="post-card-list module">
			<h2 class="post-type-name">
				<?php if ($child_cat == 'articles') echo 'Featured Articles'; ?>
				<?php if ($child_cat == 'upcoming-events') echo 'New Courses & Programs'; ?>
				<?php if ($child_cat == 'special-needs-articles') echo 'Special Needs Articles'; ?>
				<?php if ($child_cat == 'special-needs-camps') echo 'Special Needs Camps'; ?>
				<?php if ($child_cat == 'special-needs-resources') echo 'Special Needs Resources'; ?>
				<?php if ($child_cat == 'pregnancy') echo 'Pregnancy'; ?>
				<?php if ($child_cat == 'babies-&-toddlers') echo 'Babies & Toddlers'; ?>
				<?php if ($child_cat == 'health-articles') echo 'Health Articles'; ?>
				<?php if ($child_cat == 'top-family-attractions') echo 'Top Family Attractions'; ?>
				<?php if ($child_cat == 'family-fun-festivals') echo 'Family Fun Festivals in Montreal'; ?>
				<?php if ($child_cat == 'getting-around') echo 'Getting Around'; ?>
				<?php if ($child_cat == 'where-to-eat') echo 'Where To Eat'; ?>
				<?php if ($child_cat == 'where-to-stay') echo 'Where To Stay'; ?>
				<?php if ($child_cat == 'day-camps') echo 'Day Camps'; ?>
				<?php if ($child_cat == 'sleep-away-camps') echo 'Sleep Away Camps'; ?>
				<?php if ($child_cat == 'back-to-school') echo 'Back To School'; ?>
				<?php if ($child_cat == 'preschool-and-kindergarden') echo 'Preschool And Kindergarden'; ?>
				<?php if ($child_cat == 'elementary-school') echo 'Elementary School'; ?>
				<a class="btn archive pull-right more-links-btn" href="<?php echo esc_url($category_link); ?>">More</a>
			</h2>

		<?php
			foreach ($posts as $post) {
				setup_postdata($post); 
				$excluded_posts[] = $post->ID;
		?>
			<div class="col-md-4 col-sm-4 col-xs-6 padding-lr5">
				<div class="post-card adjust_recommand_post_height padding-10 border-1 radius-5 margin-b10">
					<div class="post-card-top">
						<a href="<?php echo get_the_permalink($post); ?>">
							<?php echo get_the_post_thumbnail( $post, 'blog-thumb' ); ?>
						</a>
					</div>
					<div class="post-card-bottom">
						<h4 class="post-card-title margin-t10">
							<a href="<?php echo get_permalink($post); ?>"><?php echo get_the_title($post); ?></a>
						</h4>
		
						<div class="post-card-content">
							<?php
								$content = get_the_content( $post );
								echo wp_trim_words($content, 20, '');
							?>
						</div>
					</div>
				</div>
			</div>

		<?php
			}
		wp_reset_postdata();
		?>
		</div>
	</div>
<?php	
	}
}
?>
