<?php
/**
 * @package themeplate
 */
global $post;

$related_posts = get_posts( array(
	'category__in' => wp_get_post_categories( $post->ID ),
	'numberposts'  => 3,
	'post__not_in' => array( $post->ID )
) );
if ( count( $related_posts ) > 1 ) :
?>
		<div class="post-card-list module">
			<div class="row margin-lr5">
				<h2 class="post-type-name padding-lr5">You Might Like</h2>
<?php
	foreach ( $related_posts as $post ) :
		setup_postdata( $post );
?>
		<div class="liked-block">
			<?php get_template_part('/loop-templates/grid-post'); ?>
		</div>
			
	<?php
		endforeach;
		wp_reset_postdata();
	?>
			</div>
		</div>
<?php
	endif; 
?>
	