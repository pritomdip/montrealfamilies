<?php
global $post;

$author_name      = get_post_meta($post->ID, 'author', true);
$event_start_date = get_post_meta($post->ID, 'startdate', true);
$event_end_date   = get_post_meta($post->ID, 'enddate', true);
$event_start_time = get_post_meta($post->ID, 'starttime', true);
$event_end_time   = get_post_meta($post->ID, 'endtime', true);

?>
<div class="col-md-12">
	<div <?php post_class('event-listing'); ?>>
		<div class="event-header">
			<h2 class="event-title">
				<a href="<?php echo get_the_permalink($post); ?>"><?php echo get_the_title($post) ?></a>
			</h2>
			<div class="clearfix"></div>
				<p class="event-date pull-left"><?php if (!empty($event_start_date)) echo mf_date_format($event_start_date) ?>
					&nbsp;-&nbsp; <?php if (!empty($event_end_date)) echo mf_date_format($event_end_date) ?>
					&nbsp;@&nbsp;<?php echo $event_start_time?>
						
					</p>
			<div class="clearfix"></div>
		</div>

		<div class="event-body">
			<div class="calendar-list-data">
				<div class="event-desc clearfix">
					<div class="calendar-list-image">
						<a href="<?php echo get_the_permalink($post); ?>">
							<?php echo get_the_post_thumbnail($post->ID, 'thumbnail'); ?>
						</a>
					</div>
					<?php
					$content = get_the_content();
					echo wp_trim_words($content, 20, '');
					?>
					<br/>
					<p class="event-detail">
						<a href="<?php echo get_the_permalink($post); ?>">
							<strong>View event details</strong>
						</a>
					</p>

					<div class="event-categories">
						<?php
							$tags = get_the_terms(get_the_ID(), 'event_category');

							if (!empty($tags)) {
								$tag_names = wp_list_pluck($tags, 'name', 'term_id');
								foreach ($tag_names as $key => $tag_name) {
									?>

									<a href="<?php echo get_tag_link($key); ?>"><?php echo $tag_name; ?></a>
								<?php }
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
