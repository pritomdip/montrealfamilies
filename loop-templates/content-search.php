<?php
/**
 * @package themeplate
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <header class="entry-header search-result-name-wrapper">

		<?php the_title( sprintf( '<h2 class="entry-title"><a class="search-result-name" href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<?php if ( 'post' == get_post_type() ) : ?>

            <div class="entry-meta">

				<?php themeplate_posted_on(); ?>

            </div><!-- .entry-meta -->

		<?php endif; ?>

    </header><!-- .entry-header -->

    <div class="entry-summary">
        <div class="modules">
            <div class="post-card-top">
               <?php
                    if ( has_post_thumbnail() ) { // check if the post Thumbnail
                        the_post_thumbnail('thumbnail');
                    } else {
                        //your default img
                    }
                ?>
                <img src="">
            </div>
            <div class="post-card-bottom">
                <?php the_excerpt(); ?>
            </div>
        </div>

		

    </div><!-- .entry-summary -->

    <footer class="entry-footer">

		

    </footer><!-- .entry-footer -->

</article><!-- #post-## -->
