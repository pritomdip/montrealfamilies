<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package themeplate
 */
global $post;

$id = $post->ID;

$street  = get_post_meta( $id, 'address', true);
$address2= get_post_meta( $id, 'address2', true);
$city    = get_post_meta( $id, 'city', true);
$phone   = get_post_meta( $id, 'phone', true);
$zipcode = get_post_meta( $id, 'zipcode', true);
$state   = get_post_meta( $id, 'state', true);
$content = get_the_content($post);

$image_galleries = get_post_meta($id, '_directory_img_gallery', true);

$metas   = get_post_meta($id);

$post_type = get_post_type_object(get_post_type());
if ($post_type) {
    $directory_name = strtolower(esc_html($post_type->name));
} 

?>
<div class="margin-t20 margin-b20"></div>
<div class="directory-single-list padding-10 single-directory-content">
	<h2 class="directory-single-list-name">
		<?php echo get_the_title($post); ?>
	</h2>
	
	<div class="directory-single-content-list content-display">
		<?php if (!empty($street)) echo "<p>$street</p>"; ?>
		<?php if (!empty($address2)) echo "<p>$address2</p>"; ?>
		<?php if (!empty($city)) echo "<p>$city<span>, ". $state ."</span><span>, ". $zipcode ."</span></p>"; ?>
		<?php if (!empty($phone)) echo "<p>$phone</p>"; ?>
	</div>
	<div class="directory-single-category directory-category category-display">
		<?php 
			$post_categories = get_the_terms($post->ID, $directory_name . '_category');

			if (!empty($post_categories) && !is_wp_error($post_categories)) {
				$categories = wp_list_pluck($post_categories, 'name', 'term_id');
				?>
				<div class="directory-list-cat">
					<div class="directory-cat">Category</div>
					<div class="directory-cat-name">
						<span class="label label-info"><?php echo implode(", ", $categories); ?></span>
					</div>
				</div>

		<?php } ?>
	</div>
	
</div>

<?php if ( !empty($image_galleries )) { ?>
	<div class="directory-single-page-gallery-wrap">
		<?php
		foreach ( $image_galleries as $key => $image_gallery ) {
			$image_link = wp_get_attachment_image_src( $key, 'thumbnail' )[0];
			?>
			<div class="gallery-item">
				<a href="<?php echo $image_gallery ?>">
					<img src="<?php echo $image_link ?>" alt="gallery image<?php echo $key; ?>">
				</a>
			</div>
		<?php } ?>
	</div>
<?php } ?>

<?php if(!empty($content)){ ?>
	<div class="directory-desc-wrapper margin-t20">
		<h2 class="description">Description:</h2>
		<p><?php echo $content; ?></p>
	</div>
<?php } ?>

<?php  if(!empty($metas)){ ?>
	<div class="directory-desc-wrapper">
		<h2 class="description">Additional Information:</h2>
		<?php if(!empty($metas['zipcode'][0])){ ?>
			<div class="margin-b20">
				<span>Zipcode: </span><p><?php echo $metas['zipcode'][0]; ?></p>
			</div>
		<?php } ?>

		<?php if(!empty($metas['website'][0])){ ?>
			<div class="margin-b20">
				<span>Website: </span>
				<p><a href="<?php echo $metas['website'][0]; ?>"><?php echo $metas['website'][0]; ?></a></p>
			</div>
		<?php } ?>	
	</div>
<?php } ?>
