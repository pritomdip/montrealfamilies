<?php
/**
 * @package themeplate
 */

	if(empty($title)) {
		return;
	}
	
?>

<div class="col-md-4 col-sm-4 col-xs-6 padding-lr5">
	<div class="post-card adjust_recommand_post_height padding-10 border-1 radius-5 margin-b10">
		<div class="post-card-top">
		<?php if(!empty($image)) { ?>
			<a href="<?php echo $link; ?>">
				<img width="230" height="172" src="<?php echo get_template_directory_uri() . $image ?>" class="attachment-blog-thumb size-blog-thumb wp-post-image">	
			</a>
		<?php } ?>
		</div>
		<div class="post-card-bottom">
			<h4 class="post-card-title margin-t10"><a href="<?php echo $link; ?>"><?php echo $title; ?></a></h4>
		<?php if(!empty($content)) { ?>
			<div class="post-card-content"><?php echo $content; ?></div>
		<?php } ?>
		</div>
	</div>
</div>
