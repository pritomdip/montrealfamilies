<?php
/**
 * The template for displaying all single posts.
 *
 * @package themeplate
 */

get_header(); ?>
<div class="wrapper" id="single-wrapper">
    
    <div id="content" class="container">

        <div class="row">
            <div class="col-md-12">
                <?php
                    get_template_part( 'page-templates/directory/directory-top-header' );

                    $post_type = get_post_type_object(get_post_type());
                    if ($post_type) {
                        $directory_name      = esc_html($post_type->labels->singular_name);
                        $directory_post_type = esc_html($post_type->name);
                    } 
                    
                ?>
                <div class="directory-heading-wrapper directory-single-page">
                    <div class="directory-name-panel">
                        <h1 class="directory-name"><?php echo $directory_name; ?></h1>
                    </div>
                    <div class="search-bar">
                        <?php 
                            $category_link = get_post_type_archive_link( $directory_post_type );
                        ?>
                        <a class="directory-search-button" href="<?php echo $category_link; ?>">← Return to search</a>
                    </div>
                </div>
            </div>
            <div id="primary" class="col-md-8 content-area">
                
                <main id="main" class="site-main" role="main">

                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php get_template_part( 'loop-templates/directory', 'single' ); ?>

                        <?php
                        // If comments are open or we have at least one comment, load up the comment template
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;
                        ?>
                        
                    <?php endwhile; // end of the loop. ?>

                </main><!-- #main -->
                
            </div><!-- #primary -->
            <div class="col-md-4">

                <?php get_template_part( 'page-templates/google', 'map' );  ?>
                <?php dynamic_sidebar('sidebar-4'); ?>
                
            </div>

        </div><!-- .row -->
        
    </div><!-- Container end -->
    
</div><!-- Wrapper end -->

<?php get_footer();