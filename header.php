<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package themeplate
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site">
    <!-- =============== Logo pannel starts ============================-->
    <div class="mf-header-wrapper">
        <div class="container">
            <div class="row mf-header-padding">
                <div class="col-md-5">
                    <div class="mf-logo">
                    <?php
                        if( function_exists( 'the_custom_logo' ) ) {
                            if( has_custom_logo() ) {
                                the_custom_logo();
                            } else {
                        ?>
                        <a class="navbar-text-color navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
                    <?php } } ?>
                    </div>
                </div>
                <div class="col-md-7">
                    <ul class="mf-service-menu">
                        <li class="mf-service-menu-item">
                            <a href="http://montrealfamilies.webpublisherpro.com/about-us/">About Us</a>
                        </li>
                        <li class="mf-service-menu-item">
                            <a href="http://montrealfamilies.webpublisherpro.com/advertise/">Advertise</a>
                        </li>
                        <li class="mf-service-menu-item">
                            <a href="http://montrealfamilies.webpublisherpro.com/contact-us/">Contact Us</a>
                        </li>
                        <li class="mf-service-menu-item">
                            <a href="https://www.facebook.com/MontrealFamilies/">
                               <i class="fa fa-facebook service-icon fb-color"></i></a>
                        </li>
                        <li class="mf-service-menu-item">
                            <a href="https://twitter.com/MtlFamilies">
                               <i class="fa fa-twitter service-icon twitter-color"></i>
                            </a>
                        </li>
                        <li class="mf-service-menu-item">
                            <a href="https://www.youtube.com/channel/UCGcgmCpSpvQQNd_2ECJ-Hug">
                               <i class="fa fa-youtube service-icon youtube-color"></i>
                            </a>
                        </li>
                        <li class="mf-service-menu-item">
                            <a href="http://visitor.r20.constantcontact.com/manage/optin?v=001lFrk-nHJzIJCj9NqFp1axE8UI-QznXILeBs41f71gCDqmaenY22dbU3P7qV_eN_0cLYO1uIAtc3Sp7_Srq3kuVJNQnToz8YC" target="_blank"><span class="newsletter">Newsletter Sign Up</span></a>
                        </li>
                    </ul>
                    <div class="mf-searchbar pull-right">
                        <form id="searchform" method="get" class="input-group" action="">
                            <div>
                                <input type="text" class="field" name="s" id="s" />
                                    <button class="btn search-btn" type="submit" id="searchbutton" name="Search">
                                        <i class="fa fa-search"></i>
                                    </button>  
                                
                                <input type="hidden" name="post_type" value="post">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ******************* navigation area ******************* -->
    <div class="wrapper-fluid wrapper-navbar" id="wrapper-navbar">

        <a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'themeplate' ); ?></a>

        <nav class="site-navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">

            <div class="navbar">

                <div class="container">

                    <div class="row">

                        <div class="col-xs-12">

                            <div class="navbar-header">
                                
                                <!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <!-- The WordPress Menu goes here -->
							<?php wp_nav_menu(
								array(
									'theme_location'  => 'main_menu',
									'container_class' => 'collapse navbar-collapse navbar-responsive-collapse main-menu',
									'menu_class'      => 'nav navbar-nav col-md-12',
									'fallback_cb'     => '',
									'menu_id'         => 'main-menu',
									'walker'          => new wp_bootstrap_navwalker()
								)
							); ?>

                        </div> <!-- .col-md-11 or col-md-12 end -->

                    </div> <!-- .row end -->

                </div> <!-- .container -->

            </div><!-- .navbar -->

        </nav><!-- .site-navigation -->

    </div><!-- .wrapper-navbar end -->

    <div class="mf-secondary-navbar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   <?php wp_nav_menu(
                        array(
                            'theme_location'  => 'secondary',
                            'container_class' => 'collapse navbar-collapse navbar-responsive-collapse secondary-menu',
                            'menu_class'      => 'nav navbar-nav col-md-12',
                            'fallback_cb'     => '',
                            'menu_id'         => 'secondary-menu',
                            'walker'          => new wp_bootstrap_navwalker()
                        )
                    ); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="mf-repeat-field">
       
    </div>

    <div class="mobile-navbar">  
        <div class="container">
            <div class="row">
                <div class="col-xs-8">
                    <div class="col-xs-4">
                        <button class="btn visible-xs mobile-btn height-style1" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                            Menu <span class="caret"></span></button>
                    </div>

                    <div class="col-xs-4">
                        <button class="btn visible-xs mobile-btn height-style2" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            More <span class="caret"></span></button>
                    </div>

                    <div class="col-xs-4">
                        <button class="btn visible-xs mobile-btn height-style3" type="button" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                            Service <span class="caret"></span></button>
                    </div>
                </div>
                <div class="col-xs-4">
                    <button class="btn visible-xs mobile-btn height-style4" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                        Search <span class="caret"></span></button>
                </div>
            </div>
        </div>
    </div>

    <div class="mobile-navbar-expand">  
        <div class="collapse mbl-menu height-style1" id="collapse1">
            <div class="well"></div>
        </div>

        <div class="collapse mbl-menu height-style2" id="collapse2">
            <div class="well"></div>
        </div>

        <div class="collapse mbl-menu height-style3" id="collapse3">
            <div class="well"></div>
        </div>

        <div class="collapse mbl-menu height-style4" id="collapse4">
            <div class="well"></div>
        </div>
    </div>
</div>
      