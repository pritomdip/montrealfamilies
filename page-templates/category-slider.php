<?php

global $cat_name, $excluded_posts;

?>

<div class="margin-t20 slider-parent cat-slider-image">
	<div class="post-slider">
		<?php
			$excluded_posts = [];
			$post_args = array(
							'posts_per_page'   => 7,
							'category_name'    => $cat_name,
							'orderby'          => 'publish_date',
							'order'            => 'DESC',
							'exclude'          =>  $excluded_posts,
							'meta_key'         => '_featured_post',
							'meta_value'       => 'true',
							'post_type'        => 'post',
							'post_status'      => 'publish',
						 );

			$posts = get_posts( $post_args );

			if(!empty($posts)){

				foreach ($posts as $post){
					setup_postdata($post);
					$excluded_posts[]   = $post->ID;
		?>
		<div class="slider-image-wrapper <?php echo $post->ID; ?>" data-id="<?php echo $post->ID; ?>">
			<a href="<?php echo get_the_permalink($post); ?>">
				<?php echo get_the_post_thumbnail( $post->ID, 'slider-thumbnail' ); ?>
			</a>
		</div>
		<?php
				}
			wp_reset_postdata();
			}
		?>
	</div>
</div>
<div class="margin-t20 slider-content-title">
	<ul class="mf-cat-sllider">
		<?php
		 $count = 0;
			foreach ( $posts as $post ) {
			    setup_postdata($post);
			    
				$excluded_posts[]   = $post->ID;		
		?>					
			<li class="cat-slider-item" data-id="<?php echo $count; ?>">		<?php echo get_the_title($post); ?>
			</li>
		<?php 
			$count++;
			} 
		wp_reset_postdata();
		?>
	</ul>
</div>