<div class="sidebar-image-wrapper">
    <div class="sidebar-image">
        <img src="<?php echo bloginfo('template_directory') ?>/assets/images/cover.jpeg">
        <div class="sidebar-image-content">
            <h4 class="present-issue"><a href="">Current Issue</a></h4>
            <p class="past-issue"><a href="">Past Issues</a></p>
            <p class="past-issue"><a href="">Subscribe</a></p>
            <p class="find-copy"><a href="">Find a Copy</a></p>
        </div>
    </div>
</div>