<?php
	$post_type = get_post_type_object(get_post_type());
	if ($post_type) {
	    $directory_name = esc_html($post_type->labels->singular_name);
	} 
?>
<div class="directory-heading-wrapper">
	<div class="directory-name-panel">
		<h1 class="directory-name"><?php echo $directory_name; ?></h1>
	</div>
	<div class="search-bar">
        <form id="searchform" method="get" class="input-group" action="">
            <div>
                <input type="text" class="field" name="keyword" id="keyword">
                <button class="directory-btn" type="submit">Search</button>
            </div>
        </form>
	</div>
</div>