<?php
	$post_type = get_post_type_object(get_post_type());
	if ($post_type) {
	    $directory_name = esc_html($post_type->labels->singular_name);
	    $directory      = esc_html($post_type->name);
	}
?>

<ul class="single-page-top-pannel">
    <li class="single-page-top-pannel-listing">
        <a href="<?php echo home_url(); ?>">Montreal Families</a> <span class="divider">/</span>
    </li>         
    <li class="single-page-top-pannel-listing">
        <?php echo $directory_name; ?> Directory
    </li>           
</ul>

<div class="module-grid directory-heading-wrapper">
	<div class="module-top-panel directory-name-panel">
		<h1 class="directory-name oswald margin-b20"><?php echo $directory_name; ?> Directory</h1>
	</div>
	<div class="module-bottom-panel modules">
		<?php 
			switch ( $directory ) {
			    case "babies":
			?>
				<div class="module-image-wrapper post-card-top">
					<img src="<?php echo get_template_directory_uri() ?>/assets/images/Montrealbabies.jpg">
				</div>
				<div class="module-content post-card-bottom babies">
					<p>There’s no doubt about it … if you have (or are having) a baby, you need a ton of stuff!
					Cribs, strollers, car seats, clothing, diapers, breastfeeding support, playgroups and more.</p>
					<p>The Montreal Families’ Resource Directory, which covers the Greater Montreal area, can help you find everything you need for your baby! Happy shopping!</p>
				</div>
			<?php
			        break;
			    case "birthday":
			?>
				<div class="module-image-wrapper post-card-top">
					<img src="<?php echo get_template_directory_uri() ?>/assets/images/birthday.jpg">
				</div>
				<div class="module-content post-card-bottom birthday">
					<p>There is so much to think about when planning your child's birthday parties.From entertainers to food, venues to loot bags, cupcakes to clowns, and magicians to bouncy castles.</p>
					<p> Our Resource Directory is the perfect tool to help you find everything you need to pull off the perfect kids' birthday party and create wonderful memories!</p>
				</div>
			<?php
			        break;
			    case "camps":
			?>
			        <div class="module-image-wrapper post-card-top">
						<img src="<?php echo get_template_directory_uri() ?>/assets/images/camps.jpg">
					</div>
					<div class="module-content post-card-bottom camps">
						<p>If you are looking for a summer camp for your child, our Camp Directory is the perfect tool to help you find it!</p>
						<p> Our listings include day camps in the Great Montreal area, specialized camps and residential camps in Quebec, Ontario and the United States.</p>
						<p> Find a camp that offers activities such as swimming, music, arts, sports, outdoor adventure, sports and so much more.</p>
						<p> There is a summer camp for every interest and age group!</p>
					</div>
			<?php
			        break;
			        case "childcare":

			?>
			        <div class="module-image-wrapper post-card-top">
						<img src="<?php echo get_template_directory_uri() ?>/assets/images/childcare.jpg">
					</div>
					<div class="module-content post-card-bottom childcare">
						<p>Entrusting your little one to someone other than immediate family or close friends is one of the most stressful aspects of parenting.</p>
						<p>We hope our Resource Directory can make that task a bit easier by listing a wide array of childcare options, including daycares, babysitting and nanny services, as well as other childcare resources.</p><p>Take a look!</p>
					</div>
			<?php
			        break;
			        case "course":
			?>
			        <div class="module-image-wrapper post-card-top">
						<img src="<?php echo get_template_directory_uri() ?>/assets/images/course.jpg">
					</div>
					<div class="module-content post-card-bottom course">
						<p>Do you have a budding actor or a sports star in the making? Maybe a young Picasso or John Lennon?</p>

						<p>Whatever your child’s interests may be, finding extracurricular activities to keep them active, interested and involved is important to parents.</p>

						<p>Take a look through our Resource Directory for an amazing list of local businesses and organizations that offer courses and programs for kids in the greater Montreal area. Music, arts, language, sports, cooking, drama and so much more!</p>
					</div>
			<?php
			        break;
			        case "education":
			?>
			        <div class="module-image-wrapper post-card-top">
						<img src="<?php echo get_template_directory_uri() ?>/assets/images/education.jpg">
					</div>
					<div class="module-content post-card-bottom education">
						<p>Parents in Quebec are faced with many decisions when it comes to their child’s education. French, English, Bilingual or Tri-lingual? Private, public, or International?</p>

						<p>To help parents make these tough choices, we’ve put together an amazing list of school boards, private and public schools, tutors along with other educational resources.</p>

						<p>Whether you are looking at preschools, Kindergartens, High Schools, or Adult Continuing Education in the greater Montreal area, our Resource Directory is the perfect tool!</p>
					</div>
			<?php
			        break;
			        case "family":
			?>
			        <div class="module-image-wrapper post-card-top">
						<img src="<?php echo get_template_directory_uri() ?>/assets/images/familyfun.jpg">
					</div>
					<div class="module-content post-card-bottom family">
						<p>Montreal, Laval, the West Island and the South Shore offer so many fun activities, festivals, special events and celebrations for families and we list the majority of them all in our Resource Directory.</p>

						<p>Whether it’s Spring, Summer, Winter or Fall, there is something fun to do for every age and interest. This list includes major attractions, museums, city parks, libraries, festivals, apple picking, ski hills, water parks, children’s theatre and so much more.</p>

						<p>So, get out there and have some FUN!</p>
					</div>
			<?php
			        break;
			        case "familyservices":
			?>
			        <div class="module-image-wrapper post-card-top">
						<img src="<?php echo get_template_directory_uri() ?>/assets/images/familyservice.jpg">
					</div>
					<div class="module-content post-card-bottom familyservices">
						<p>When families and individuals need help, finding the right information and local resources can be tough.</p>

						<p>Our Family Services directory offers a wealth of information about  important issues, such as caring for seniors, what to do in the case of elder abuse, places to turn when dealing with domestic violence, community agencies that offer families a helping hand, anti-bullying information for parents,  addiction support and much more.</p>
					</div>
			<?php
			        break;
			        case "health":
			?>
			        <div class="module-image-wrapper post-card-top">
						<img src="<?php echo get_template_directory_uri() ?>/assets/images/healthwell.jpg">
					</div>
					<div class="module-content post-card-bottom health">
						<p>Simply put, there is nothing more important than the health and wellbeing of your family.</p>

						<p>So, Montreal Families has compiled listings of hundreds of local health resources, such as cancer support, child safety, depression, allergy resources, mental health and counselling services, nutrition, pediatric clinics, speech therapy and much more.</p>
					</div>
			<?php
			        break;
			        case "prepost":
			?>
			        <div class="module-image-wrapper post-card-top">
						<img src="<?php echo get_template_directory_uri() ?>/assets/images/prepost.jpg">
					</div>
					<div class="module-content post-card-bottom prepost">
						<p>Expecting a baby is a very exciting experience but it is also a time fraught with many questions and concerns. Soon-to-be parents want to be sure they are making the right decisions every step of the way.</p>

						<p>Our Pre & Postnatal Directory provides information about several services, such as prenatal courses and counselling, midwife and doula services, birthing centres, breastfeeding support and much more.</p>

						<p>Find everything you need to help make your pregnancy and the birth of your child a special time.</p>
					</div>
			<?php
			        break;
			        case "retail":
			?>
			        <div class="module-image-wrapper post-card-top">
						<img src="<?php echo get_template_directory_uri() ?>/assets/images/retail.jpg">
					</div>
					<div class="module-content post-card-bottom retail">
						<p>Whether you are looking for a new book, playground equipment, crafting supplies, a ballet outfit, Halloween or Purim costumes, kids’ shoes or a new toy, our list of local retail stores in Greater Montreal is bound to help you find what you need.</p>
						
						<h4>Happy shopping!</h4>
					</div>
			<?php
			        break;
			        case "needs":
			?>
			        <div class="module-image-wrapper post-card-top">
						<img src="<?php echo get_template_directory_uri() ?>/assets/images/specialneeds.jpg">
					</div>
					<div class="module-content post-card-bottom needs">
						<p>Parents with children who have special needs are constantly looking for local resources and information to help in their everyday lives.</p>

						<p>Below is a list of hundreds of businesses and organizations that offer specialized courses and programs, camps, sports and education for children with special needs. There is also a comprehensive list of support and respite services for families as well as a section specifically for families with kids on the autism spectrum.</p>

						<p>If you know of any resources that we are missing, please send us an email. </p>
					</div>
			<?php
			        break;
			    default:
			?>
		        <div class="module-image-wrapper post-card-top">
					<img src="<?php echo get_template_directory_uri() ?>/assets/images/sports.jpg">
				</div>
				<div class="module-content post-card-bottom sports">
					<p>There is no better way to keep your kids active and healthy than by having them take part in a sport they enjoy and the greater Montreal area offers many amazing sports for kids!</p>

					<p>Of course, there is hockey, ringuette, skiing, soccer and swimming but our city also offers some lesser known activities, such as fencing, baseball, martial arts, sailing and rowing, squash, horseback riding, and fitness (just to name a few).</p>

					<p>Check out all of the great options available and just maybe your child will find a new passion! </p>
				</div>
			<?php
			}
		?>
	</div>
</div>