<?php
global $wpdb;
$table_name         = $wpdb->prefix . 'posts';

$post_type = get_post_type_object(get_post_type());
if ($post_type) {
    $directory_name = strtolower(esc_html($post_type->name));
} 
?>

<?php
	if( isset($_GET[ $directory_name . '_category']) || isset($_GET[ $directory_name .'_latter_asc']) || isset($_GET[ $directory_name .'_latter_asc']) ){
?>

	<div class="directory-heading-wrapper margin-b20">
		<div class="clear-filter">          
	        <a href="<?php echo site_url() . '/' .$directory_name; ?>"><button class="directory-btn" type="submit">Clear Filters</button></a>
		</div>
	</div>

<?php
	}
?>

<div class="directory-filter hidden-sm hidden-xs">
	<h5 class="directory-filter-title">Category</h5>
	<ul class="directory-filter-list listing-categories directory-category-menu">
	</ul>
</div>

<div class="directory-filter hidden-sm hidden-xs">
	<h5 class="directory-filter-title">Name</h5>
	<ul class="directory-filter-list listing-alphabets directory-category-menu">
	</ul>
</div>


<div class="directory-filter directory-hidden-form hidden-md hidden-lg">
	<h5 class="directory-filter-title directory-search-toggle-btn">ADVANCE SEARCH</h5>
	<form action="" method="get" class="directory-filter-form directory-slide" id="listing-filter-form">
		<div class="directory-search-form-wrap">
			<label>Categories : </label>
			<select name="<?php echo $directory_name ?>_category" id=""
					class="category_listing_select selectpicker">
			<?php
				$listing_categories =   get_terms(
											array(
												'taxonomy'   => $directory_name . '_category',
												'hide_empty' => true,
												array(
													'order'  => 'ASC',
													'orderby'=> 'name',
												),
											)
									    );
				if (!empty($listing_categories)) {
				   $listing_categories = wp_list_pluck($listing_categories, 'name', 'slug');
					var_dump($listing_categories);
				}
					echo "<option value=''>ALL</option>";
				foreach ($listing_categories as $slug => $term_name) {
					echo "<option value='{$slug}'>{$term_name}</option>";
				}
			?>
			</select>
		</div>

		<div class="directory-search-form-wrap">
			<select name="<?php echo $directory_name; ?>_latter_asc" class="alphabet_listing_select hide">
				<?php
				$prepared_statement = $wpdb->prepare( "SELECT distinct(LEFT(post_title, 1)) FROM {$table_name} WHERE post_title != '' AND  post_type = %s order by post_title ;", $directory_name );
				$alphabet_lists     = $wpdb->get_col( $prepared_statement );
				
				echo "<option value=''>ALL</option>";
				foreach ( $alphabet_lists as $alphabet_key => $alphabet_name ) {
					echo "<option value='{$alphabet_name}'>{$alphabet_name}</option>";
				}
				?>
			</select>
		</div>

		<input type="hidden" name="directory_name" value="<?php echo $directory_name; ?>">
		<input type="hidden" name="keyword" value="<?php echo empty($_GET['keyword']) ? '' : esc_attr($_GET['keyword']);  ?>">
		<!-- <input type="hidden" name="post_type" value="<?php echo $directory_name;  ?>"> -->
		<input type="hidden" id="paged" name="paged" value="1">
	</form>
</div>