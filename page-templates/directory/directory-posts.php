<?php 
$exclude_posts = [];

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$post_type = get_post_type_object(get_post_type());
if ($post_type) {
    $directory_name = strtolower(esc_html($post_type->name));
    $direc_name 	= strtolower(esc_html($post_type->name));
} 

if (!empty($directory_name)) {

	if(isset($_GET[ $directory_name . '_category']) && $_GET[ $directory_name . '_category'] !='') {
		$tax_query_attr = array(
	        array(
	            'taxonomy' => $directory_name . '_category',
	            'field'    => 'slug', 
	            'terms'    => sanitize_text_field($_GET[ $directory_name . '_category'])
	        )
	    );
	} else {
		$tax_query_attr = '';
	}

	if(isset($_GET[ $directory_name .'_latter_asc']) && $_GET[ $directory_name . '_latter_asc'] !=''){
		global $wpdb;

		$search_keyword = $wpdb->esc_like($_GET[ $directory_name . '_latter_asc']);
		$search_keyword = $search_keyword .'%';

		$post_id = $wpdb->get_col("select ID from $wpdb->posts where post_type = '$directory_name' AND post_status='publish' AND post_title like '$search_keyword' ");
		
	} else if (isset($_GET['keyword']) && $_GET['keyword'] !=''){
		global $wpdb;
		$search_keyword = $wpdb->esc_like($_GET['keyword']);
		$search_keyword = $search_keyword .'%';
		$post_id = $wpdb->get_col("select ID from $wpdb->posts where post_type = '$directory_name' AND post_status='publish' AND post_title like '$search_keyword' ");

		if( empty( $post_id )){
			$directory_name = 'abcde';
			$tax_query_attr = '';
		}

	} else {
		$post_id = '';
	}

	$args = array(
	    'post_type' => $directory_name,
	    'tax_query' => $tax_query_attr,
	    'post__in'  => $post_id,
	    'showposts' => 10,
	    'posts_per_page' => 10,
		'paged'		=> $paged
	);

	$directory_query = new WP_Query($args);  

	if ($directory_query->have_posts()) { 

		while ($directory_query->have_posts()) :
			$directory_query->the_post(); 
			get_template_part('loop-templates/directory', 'content');
		endwhile; 
		wp_reset_postdata(); 
	?>
	<?php
		$count = $directory_query->post_count;
		if( $count > 9  ){
	?>

		<div class="col-md-12 padding-lr5">
            <ul class="pagination custom-pagination pull-right">
                <?php
				  global $wp_query;
				 
				  $big = 999999999; // need an unlikely integer
				  echo '<li class="paginate-links">';
				    echo paginate_links( array(
				    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				    'format' => '?paged=%#%',
				    'prev_text' => __('<span><i class="fa fa-angle-double-left"></i></span>'),
				    'next_text' => __('<span><i class="fa fa-angle-double-right"></i></span>'),
				    'current' => max( 1, get_query_var('paged') ),
				    'total' => $wp_query->max_num_pages
				    ) );
				  echo '</li>';
				?>
            </ul>
        </div>
        <div class="clearfix"></div>
    <?php } ?>

	<?php } else { ?>
		<div class="directory-single-list padding-10">
			<div class="directory-single-list-top">
				<p class="empty-results">There are no <?php echo $direc_name; ?> that match your search query.</p>
			</div>
		</div>
<?php
	} } 
