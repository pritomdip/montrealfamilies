<?php

global $excluded_posts, $current_category_id, $cat_name;

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	$args = array(
				'posts_per_page'   => -1,		
				'cat'              => $current_category_id,
				'orderby'          => 'publish_date',
				'order'            => 'DESC',
				'post_type'        => 'post',
				'post_status'      => 'publish',
				'showposts'		   => 10,
				'paged'			   => $paged
			);
$query         = new WP_Query($args);

?>
	<div class="col-md-12">
		<div class="directory-heading-wrapper">
			<div class="directory-name-panel">
				<h2 class="directory-name"><?php echo $cat_name; ?></h2>
			</div>
		</div>

		<div class="single-category-header-wrapper modules">
			<div class="left-title">Title</div>
			<div class="right-issue">Issue</div>
		</div>
		
		<?php
			while ($query->have_posts()) { $query->the_post();
				setup_postdata($post); 

				$excluded_posts[] = $post->ID;
				$subtitle = get_post_meta($post->ID, 'subtitle', true);

				if(empty($subtitle)){
					$subtitle = '';
				}
				$archive_year  = get_the_time('Y'); 
				$archive_month = get_the_time('m'); 
				$archive_day   = get_the_time('d'); 
		?>
		<div class="post-card-list modules">
			<div class="post-card-top">
				<a href="<?php echo get_the_permalink($post); ?>">
					<?php the_post_thumbnail('thumbnail'); ?>
				</a>
			</div>
			<div class="post-card-bottom">
				<div class="bottom-left">
					<?php the_title(sprintf('<h4 class="post-card-title  margin-t10"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h4>'); ?>
					<div class="publishing-date margin-t10">
						<a href="<?php echo get_day_link( $archive_year, $archive_month, $archive_day); ?>"><?php the_date('F Y'); ?></a>	
					</div>
				</div>
				<div class="post-card-content">
					<?php
						echo $subtitle;
					?>
				</div>
				<?php
					$author = get_post_meta($post->ID, 'author', true);
					if(!empty( $author )){
				?>
					<div class="author"><?php echo $author; ?></div>
				<?php
					}
				?>
				
			</div>
		</div>
		<?php
			}
			wp_reset_postdata();
		?>
	</div>


	<div class="col-md-12 padding-lr5 text-center">
		<ul class="pagination custom-pagination">  
			<?php
			  global $wp_query;
			 
			  $big = 999999999; // need an unlikely integer
			  echo '<li class="paginate-links">';
			    echo paginate_links( array(
			    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			    'format' => '?paged=%#%',
			    'prev_text' => __('<span><i class="fa fa-angle-double-left"></i></span>'),
			    'next_text' => __('<span><i class="fa fa-angle-double-right"></i></span>'),
			    'current' => max( 1, get_query_var('paged') ),
			    'total' => $wp_query->max_num_pages
			    ) );
			  echo '</li>';
			?>
        </ul>
    </div>
