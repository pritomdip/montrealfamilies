<?php
/**
 * Template Name:Event Listing Page
 * @package themeplate
 */
$excluded_posts = [];
get_header();
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

?>

<div class="wrapper" id="page-wrapper">

	<div id="content" class="container">

		<ul class="breadcrumb">
			<li>
				<a href="<?php echo get_site_url(); ?>">Montreal Families</a>
			</li>
			<li class="active">Montreal Families <?php echo get_the_title() ?></li>
		</ul>


		<div class="row">
			<div id="primary" class="col-md-8 content-area b-r-1">

				<main id="main" class="site-main" role="main">
					<!--event posts-->
					<?php
					$args = array(
						'post_type'      => 'pro_event',
						'posts_per_page' => 10,
						'post_status'    => 'publish',
						'showposts'      => 10,
						'paged'		     => $paged
					);
					$calendar_posts = new WP_Query($args);

					if ($calendar_posts->have_posts()) { 
						while ($calendar_posts->have_posts()) :
							$calendar_posts->the_post(); 
							get_template_part('loop-templates/event-list-post');
						endwhile; 
						wp_reset_postdata(); 					
					?>
				
					<div class="col-md-12">
			            <ul class="pagination custom-pagination pull-right">
			                <?php
							  global $wp_query;
							 
							  $big = 999999999; // need an unlikely integer
							  echo '<li class="paginate-links">';
							    echo paginate_links( array(
							    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							    'format' => '?paged=%#%',
							    'prev_text' => __('<span><i class="fa fa-angle-double-left"></i></span>'),
							    'next_text' => __('<span><i class="fa fa-angle-double-right"></i></span>'),
							    'current' => max( 1, get_query_var('paged') ),
							    'total' => $wp_query->max_num_pages
							    ) );
							  echo '</li>';
							?>
			            </ul>
			        </div>
				<?php } ?>
			        <div class="clearfix"></div>

				</main><!-- #main -->

			</div><!-- #primary -->

			<div id="secondary" class="col-md-4 widget-area" role="complementary">
				<div class="sidebar home-sidebar">
					<?php dynamic_sidebar('sidebar-1'); ?>
				</div>
			</div><!-- #secondary -->

		</div><!-- .row -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
