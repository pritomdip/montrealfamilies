<div class="newsletter-wrapper">
    <div class="post-type-name">Top Weekend Picks</div>
    <div class="sidebar-newsletter-content-wrapper">
        <p class="newsletter-content">Every Wednesday, we send out a newsletter that highlights the most fun upcoming activities for the family. If it’s happening around Montreal and is fun for kids, we’ve got it covered. We also have contests with great prizes. Don’t miss out – it’s fast and easy to sign up.</p>
        <div class="newsletter-btn">
            <a class="newsletter-link" href="">Newsletter Sign-Up</a>
        </div>
    </div>                   
</div>