<?php
    global $post;
    global $excluded_posts;
    $trending_posts = get_posts( array( 
        'posts_per_page' => 3, 
        'meta_key'       => 'mf_post_views_count', 
        'orderby'        => 'meta_value_num', 
        'order'          => 'DESC',
        'post__not_in'   => $excluded_posts
    ) );
    //error_log('lksnkl');
    if ( ! empty( $trending_posts ) ) {
        ?>
        <div class="related-posts trending">
            <div class="post-type-name">Most Popular Articles</div> 
            <div class="popular-posts-wrapper">    
                <?php foreach ( $trending_posts as $post ): ?>
                    <?php setup_postdata( $post ); ?>
                    <div class="liked-block border-b1">
                        <?php //get_template_part( 'loop-templates/grid', 'post' ); ?>

                        <div class="paddin5">
                            <div class="post-card module new-module adjust_recommand_post_height padding-10 border-1 radius-5 margin-b10">
                                <div class="post-card-top module-top">
                                    <?php the_title(sprintf('<h4 class="post-card-title  margin-t10"><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h4>'); ?>
                                </div>
                            </div>
                        </div>

                    </div>
                <?php endforeach; ?>
            </div>
        </div>
<?php } ?>
