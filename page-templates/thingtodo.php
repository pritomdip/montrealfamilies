<?php
/*
 * Template Name: Things To Do
 */
get_header();
?>
<section class="body-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-8 body-border category-page-wrapper">

                <div class="margin-t20 slider-parent cat-slider-image">
                    <div class="post-slider">
                        <?php
                            $excluded_posts = [];
                            $post_args = array(
                                            'posts_per_page'   => 5,
                                            'orderby'          => 'publish_date',
                                            'order'            => 'DESC',
                                            'exclude'          =>  $excluded_posts,
                                            'meta_key'         => '_featured_post',
                                            'meta_value'       => 'true',
                                            'post_type'        => 'post',
                                            'post_status'      => 'publish',
                                         );

                            $posts = get_posts( $post_args );

                            if(!empty($posts)){

                                foreach ($posts as $post){
                                    setup_postdata($post);
                                    $excluded_posts[]   = $post->ID;
                        ?>
                        <div class="slider-image-wrapper <?php echo $post->ID; ?>" data-id="<?php echo $post->ID; ?>">
                            <a href="<?php echo get_the_permalink($post); ?>">
                                <?php echo get_the_post_thumbnail( $post->ID, 'slider-thumbnail' ); ?>
                            </a>
                        </div>
                        <?php
                                }
                            wp_reset_postdata();
                            }
                        ?>
                    </div>
                </div>
                <div class="margin-t20 slider-content-title">
                    <ul class="mf-cat-sllider">
                        <?php
                         $count = 0;
                            foreach ( $posts as $post ) {
                                setup_postdata( $post );
                                
                                $excluded_posts[]   = $post->ID;        
                        ?>                  
                            <li class="cat-slider-item" data-id="<?php echo $count; ?>">        <?php echo get_the_title($post); ?>
                            </li>
                        <?php 
                            $count++;
                            } 
                        wp_reset_postdata();
                        ?>
                    </ul>
                </div>

                <div class="col-md-12 event-filter-page-wrapper margin-t20">
                    <?php if ( class_exists('EventCalendarPro') ) {
                        require get_template_directory() . '/page-templates/calendar/widget-events-filter.php';
                        ?>
                        <div id="community-calendar" class="gm-community-calendar-area community-calendar"></div>
                        <?php
                        require get_template_directory() . '/page-templates/calendar/calendar-event-list.php';
                    } ?>

                    <div class="ecp-widget-event-footer gm-community-calendar-sidebar-button">
                        <a href="<?php echo ecp_get_page_url('events_page'); ?>" class="btn btn-block btn-default">View All Listing
                            <img
                                src="<?php echo WPECP_ASSETS_URL . '/images/icons/right-arrow.svg' ?>" class="ecp-icon" alt=""></a>
                        <a href="<?php echo ecp_get_page_url('event_submit_page'); ?>" class="btn btn-block btn-default">Submit an event
                            <img src="<?php echo WPECP_ASSETS_URL . '/images/icons/right-arrow.svg' ?>" class="ecp-icon" alt=""></a>
                    </div>

                </div>
                <div class="clearfix"></div>

                <div class="mf-cat-posts">
                    <?php
                        echo do_shortcode('[mf_category_by_post cat="upcoming-activities-in-montreal" title="Upcoming Fun in Montreal"]');
                    ?>
                </div>

                <div class="mf-cat-posts">
                    <?php
                        echo do_shortcode('[mf_category_by_post cat="holiday-happenings" title="Holiday Happenings" limit="9"]');
                    ?>
                </div>

                <div class="mf-cat-posts">
                    <?php
                        echo do_shortcode('[mf_category_by_post cat="winter-fun" title="Winter Fun"]');
                    ?>
                </div>
            </div>
            <div class="col-md-4 margin-t20">
	            <?php 
	            	get_sidebar();
	            	
                    //echo do_shortcode('[mf_sidebar_parent_category]');
	            ?>
	        </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
