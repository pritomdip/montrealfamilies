<div class="sidebar-resource">
    <div class="post-type-name">Resource Directory</div>
    <div class="panel-group" id="accordion">
    <?php
       $directory_names = mf_get_all_post_types();         
       foreach( $directory_names as $key => $value) :
    ?>
        <!--- START PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $key; ?>"><?php echo $value; ?> <i class="fa fa-caret-down sidebar-caret-icon" aria-hidden="true"></i>
                   </a>
                </h4>
            </div>
            <div id="collapse<?php echo $key; ?>" class="panel-collapse collapse">
                <div class="panel-body">
                    <ul class="list-unstyled">
                        <li>
                            <a href="<?php echo site_url() . '/' .$key; ?>">All Listing</a>
                        </li>
                    <?php
                        $taxonomy = $key.'_category'; 
                        $terms = get_terms($taxonomy, array(
                                    'hide_empty' => false,
                                ));
                        
                        if ( $terms && !is_wp_error( $terms ) ) {
                            foreach ( $terms as $term ) { 

                                $slug = $term->slug; 
                    ?>

                                <li><a href="<?php echo site_url() . '/' . $key .'/?'. $key . '_category='. $slug;?>"><?php echo $term->name; ?></a>
                                </li>
                               
                <?php   }   }  ?>
                    </ul>
                </div>
            </div>
        </div>
        <!--   END PANEL -->
        <?php endforeach; ?>
    </div>
</div> 