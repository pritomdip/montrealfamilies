<div class="google-map-wrap mb-30">
	<?php
	global $wp_query;
	$locations = array();
	if (is_post_type_archive()) {
		$posts   = $wp_query->get_posts();
		$counter = 1;
		foreach ( $posts as $item ) {
			$title = $item->post_title;
			$title = preg_replace('/[^\p{L}\p{N}]/u', '', $title );
			$lat   = get_post_meta( $item->ID, 'latitude', true );
			$lon   = get_post_meta( $item->ID, 'longitude', true );
			if ( isset( $title ) && isset( $lat ) && isset( $lon )) {
				$locations[] = array(
					$title,
					floatval( $lat ),
					floatval( $lon ),
					$counter
				);
				$counter++;
			}
		}
	} else {
		$title = get_the_title();
		$title = preg_replace( '/[^\p{L}\p{N}]/u', '', $title );
		$lat   = get_post_meta(get_the_ID(), 'latitude', true);
		$lon   = get_post_meta(get_the_ID(), 'longitude', true);
		if ( isset( $title ) && isset( $lat ) && isset( $lon )) {
			$locations[] = array(
				$title,
				floatval( $lat ),
				floatval( $lon ),
				1
			);
		}
	}

	?>
	<div id="map" style="width: 100%; height: 400px;"></div>
</div>
<script>
	jQuery(document).ready(function ($) {
		var locations = JSON.parse('<?php echo json_encode($locations); ?>');
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 10,
			center: new google.maps.LatLng(-33.92, 151.25),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		var infowindow = new google.maps.InfoWindow();
		var marker, i;
		for (i = 0; i < locations.length; i++) {
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(locations[i][1], locations[i][2]),
				map: map
			});

			google.maps.event.addListener(marker, 'click', (function (marker, i) {
				return function () {
					infowindow.setContent(locations[i][0]);
					infowindow.open(map, marker);
				}
			})(marker, i));

			map.setCenter({lat: locations[0][1], lng: locations[0][2]});
		}
	});

</script>
