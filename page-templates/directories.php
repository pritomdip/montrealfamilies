<?php
/*
 * Template Name: Directories
 */

get_header();
?>

<section class="body-wrapper">
    <div class="container">
        <div class="row">
        	<div class="col-md-8">
	        	<div class="post-card-list module">
	        		<h2 class="post-type-name">Directories</h2>
	        	</div>
	                
	            <?php
	                echo do_shortcode('[mf_all_directories_post title="Babies" layout="grid" link="http://montrealfamilies.webpublisherpro.com/babies/" image="/assets/images/directory/babies.jpeg" content="Cribs, strollers, car seats, clothing, diapers as well as breastfeeding support and playgroups etc. Local products and services can be found here!"]');

	               echo do_shortcode('[mf_all_directories_post title="Birthday Parties" layout="grid" link="http://montrealfamilies.webpublisherpro.com/birthday/" image="/assets/images/directory/birthday.jpeg" content="From entertainment and venues to cupcakes and bouncy castles, there is a lot to consider when planning a party. This directory is the perfect tool to help pull off a memorable event."]');

	               echo do_shortcode('[mf_all_directories_post title="Camps" layout="grid" link="http://montrealfamilies.webpublisherpro.com/camps/" image="/assets/images/directory/Camps.jpeg" content="Music, arts, sports, outdoor adventure, martial arts, etc. Listings of camps: day, residential (sleepover), family and special needs as well as camp associations."]');

	               echo do_shortcode('[mf_all_directories_post title="Childcare" layout="grid" link="http://montrealfamilies.webpublisherpro.com/childcare/" image="/assets/images/directory/Childcare.jpeg" content="Daycares, babysitting and nanny services, as well as other resources and organizations that offer a Parents Night Out. Take a look."]');

	               echo do_shortcode('[mf_all_directories_post title="Courses & Programs" layout="grid" link="http://montrealfamilies.webpublisherpro.com/course/" image="/assets/images/directory/courses.jpeg" content="Courses and programs for kids in music, arts & crafts, language, cooking, drama & theatre, choir, etc."]');

	               echo do_shortcode('[mf_all_directories_post title="Education" layout="grid" link="http://montrealfamilies.webpublisherpro.com/education/" image="/assets/images/directory/education.jpeg" content="School boards, private schools, preschools, as well as tutors and other educational resources, such as high school exam preparation and home schooling."]');

	               echo do_shortcode('[mf_all_directories_post title="Family Fun" layout="grid" link="http://montrealfamilies.webpublisherpro.com/family/" image="/assets/images/directory/familyfun.jpeg" content="Festivals, major & historical attractions, museums, city parks, libraries, apple and berry picking, ski hills, water parks, indoor play centres, farms with activities, children’s theatre etc."]');

	               echo do_shortcode('[mf_all_directories_post title="Family Services" layout="grid" link="http://montrealfamilies.webpublisherpro.com/familyservices/" image="/assets/images/directory/familyservices.jpeg" content="Festivals, major & historical attractions, museums, city parks, libraries, apple and berry picking, ski hills, water parks, indoor play centres, farms with activities, children’s theatre etc."]');

	               echo do_shortcode('[mf_all_directories_post title="Health & Well Being" layout="grid" link="http://montrealfamilies.webpublisherpro.com/health/" image="/assets/images/directory/health.jpeg" content="Child safety, cancer support, depression, allergy resources, mental health and counselling services, nutrition, speech therapy and much more."]');

	                echo do_shortcode('[mf_all_directories_post title="Pre & Postnatal" layout="grid" link="http://montrealfamilies.webpublisherpro.com/prepost/" image="/assets/images/directory/prepost.jpeg" content="Prenatal courses and counselling, doula services, birthing centres, breastfeeding support, breast pump rental and sale, etc."]');

	                echo do_shortcode('[mf_all_directories_post title="Retail" layout="grid" link="http://montrealfamilies.webpublisherpro.com/retail/" image="/assets/images/directory/retail.jpeg" content="Looking for childrens books, playground equipment, crafting supplies, dance outfits, costumes, kids’ shoes or a new toy? We have got it covered."]');

	                echo do_shortcode('[mf_all_directories_post title="Special Needs" layout="grid" link="http://montrealfamilies.webpublisherpro.com/needs/" image="/assets/images/directory/specialneeds.jpeg" content="Courses and programs, camps, sports and schools for kids with special needs. There is also support and respite services for families."]');

	                 echo do_shortcode('[mf_all_directories_post title="Sports" layout="grid" link="http://montrealfamilies.webpublisherpro.com/sports/" image="/assets/images/directory/sports.jpeg" content="Courses and programs in hockey, sailing & rowing, ringuette, skiing, soccer, swimming, fencing, baseball, martial arts, squash, horseback riding, fitness, etc."]');
	            ?>
	        </div>
            <div class="col-md-4">
            	<?php get_template_part( 'loop-templates/content', 'notice' );  ?>
			    <?php get_template_part( 'loop-templates/content', 'notice' );  ?>
			    <?php get_template_part( 'loop-templates/content', 'notice' );  ?>

			    <!-- ==== category directory starts      ============= -->
				<?php get_template_part( 'page-templates/sidebar-cat-directories' ); ?>
				<!-- ==== category directory ends        ==============-->
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
