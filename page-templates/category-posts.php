<?php 

global $excluded_posts, $current_category_id, $cat_name;

$parent_cat_arg         = array('hide_empty' => true, 'parent' => $current_category_id );
$multiple_child_cat     = get_terms('category', $parent_cat_arg);

foreach ( $multiple_child_cat as $child_cat ){
	setup_postdata($child_cat);
	
	$args = array(
				'posts_per_page'   => 6,		
				'category'         => $child_cat->term_id,
				'orderby'          => 'publish_date',
				'order'            => 'DESC',
				'exclude'          => '',
				'post_type'        => 'post',
				'post_status'      => 'publish',
			);
	$child_cat_posts = get_posts( $args );
	$category_link   = get_category_link($child_cat);

	if(!empty($child_cat_posts)) {
?>
<div class="col-md-12">
	<div class="post-card-list module">
		<h2 class="post-type-name"><?php echo $child_cat->name; ?>
			<a class="btn archive pull-right more-links-btn" href="<?php echo esc_url($category_link); ?>">More</a>
		</h2>
	</div>
	<?php
		foreach ($child_cat_posts as $post) {
			setup_postdata($post); 
			$excluded_posts[] = $post->ID;
			get_template_part( 'loop-templates/grid', 'post' ); 
		} 
	?>
</div>
<?php 
	}
} 
wp_reset_postdata();
?>