<?php
/*
 * Template Name: Camps
 */
get_header();
?>
<section class="body-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-8 body-border">
                <div class="mf-cat-posts">
                    <?php
                        echo do_shortcode('[mf_all_categories_featured_post_by_cat_name cat_name="camps" child_cat="day-camps,sleep-away-camps,special-needs-camps"]');
                    ?>
                </div>
            </div>
            <div class="col-md-4">
	            <?php 
	            	get_template_part( 'loop-templates/content', 'notice' ); 
    				get_template_part( 'loop-templates/content', 'notice' ); 
    				get_template_part( 'loop-templates/content', 'notice' ); 
	            	get_template_part( 'page-templates/sidebar-newsletter' );
                    get_template_part( 'page-templates/sidebar-cat-directories' );
	            	
                    echo do_shortcode('[mf_sidebar_parent_category]');
	            ?>
	        </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
