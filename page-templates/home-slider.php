<div class="main-slider-wrapper">
<!-- <div id="overlayer"></div>
<span class="loader">
  <span class="loader-inner"></span>
</span> -->
	<div class="main-slider">
		<?php
			$post_args = array(
							'posts_per_page'   => 4,
							'orderby'          => 'publish_date',
							'order'            => 'DESC',
							'meta_key'         => '_has_slider',
							'meta_value'       => 'true',
							'post_type'        => 'post',
							'post_status'      => 'publish',
						 );

			$posts = get_posts( $post_args );
		if( ! empty($posts)){
			foreach ($posts as $post){
				setup_postdata($post);
				$excluded_posts[]   = $post->ID;
				$has_meta_link      = get_post_meta($post->ID, '_slide_post', true);

				if(empty($has_meta_link)){
					$link = get_the_permalink($post); 
				} else {
					$link = get_post_meta($post->ID, '_slide_post', true);
				}			
		?>
			<div class="main-slider-content-wrapper <?php echo $post->ID; ?>">
				<a href="<?php echo $link; ?>">
					<?php echo get_the_post_thumbnail( $post->ID, 'home-slider' ); ?>
				</a>
			</div>

		<?php
			}
		wp_reset_postdata();
		}
	?>
	</div>
</div>