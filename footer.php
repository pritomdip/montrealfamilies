<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package themeplate
 */
?>
<div class="wrapper footer-wrapper-border" id="wrapper-footer">

    <div class="container">

        <div class="row">
            <div class="col-md-3 sidebar-1">
				<?php dynamic_sidebar( 'footer-1' ); ?>
            </div>
            <div class="col-md-3 sidebar-2">
				<?php dynamic_sidebar( 'footer-2' ); ?>
            </div>
            <div class="col-md-5">
				<?php dynamic_sidebar( 'footer-3' ); ?>
                <div class="textwidget design-by">
                    <p>Website by <a href="https://webpublisherpro.com" target="_new" >Web Publisher PRO</a></p>
                </div>
            </div>
        </div>

        <!-- <div class="row">

            <div class="col-md-12">

                <footer id="colophon" class="site-footer" role="contentinfo">

                    <div class="site-info">
                        <a href="<?php 
                        // echo esc_url( __( 'http://wordpress.org/', 'themeplate' ) ); 
                        ?>"><?php 
                        // printf( __( 'Proudly powered by %s', 'themeplate' ), 'WordPress' ); ?></a>
                        <span class="sep"> | </span>
						// <?php 
                        // printf( __( 'Theme: %1$s by %2$s.', 'themeplate' ), 'themeplate', '<a href="http://themeplate.com/" rel="designer">themeplate.com</a>' ); 
                        ?>
                    </div>

                </footer>
            </div> -->

        <!-- </div> -->

    </div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>
